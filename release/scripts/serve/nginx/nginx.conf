user www-data;
worker_processes auto;
worker_rlimit_nofile 200000;

error_log /var/log/nginx/error.log warn;
pid /var/run/nginx.pid;

events {
    worker_connections  8000;
    multi_accept on;
    use epoll;
}

http {
  include /etc/nginx/mime.types;
  default_type application/octet-stream;
  server_tokens off;

  map $http_upgrade $connection_upgrade {
       default upgrade;
        ''      close;
  }

  proxy_buffer_size   128k;
  proxy_buffers   4 256k;
  proxy_busy_buffers_size   256k;
  proxy_intercept_errors on;

  keepalive_timeout  65;
  client_body_timeout 60;
  client_max_body_size 2048M;
  server_names_hash_bucket_size 64;
  types_hash_max_size 2048;
  reset_timedout_connection on;
  send_timeout 60;
  proxy_read_timeout 500;
  proxy_connect_timeout 500;

  log_format main '$remote_addr - $remote_user [$time_local] "$request" '
		  '$status $body_bytes_sent "$http_referer" '
		  '"$http_user_agent" "$http_x_forwarded_for"';

  access_log /var/log/nginx/access.log main;

  sendfile on;
  tcp_nopush on;

  gzip on;
  gzip_proxied any;
  gzip_min_length 256;
  gzip_comp_level 4;
  gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
  open_file_cache max=65000 inactive=20s;
  open_file_cache_valid 30s;
  open_file_cache_min_uses 2;
  open_file_cache_errors on;

  client_body_buffer_size 128k;

  include /etc/nginx/conf.d/*;
}