docker exec -it dist_module-user_1 php vendor/robmorgan/phinx/bin/phinx migrate
docker exec -it dist_module-user_1 php vendor/robmorgan/phinx/bin/phinx seed:run

docker exec -it dist_module-permission_1 php vendor/robmorgan/phinx/bin/phinx migrate
docker exec -it dist_module-permission_1 php vendor/robmorgan/phinx/bin/phinx seed:run