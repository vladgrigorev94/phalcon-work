<?php

namespace ServiceClientUser\Services;

use ServiceClient\Core\Response;
use ServiceClient\Core\Service;
use ServiceClientUser\Core\UserApi;

/**
 * Class UserService
 * @package ServiceClientUser\Services
 */
class UserService extends Service
{
    /**
     * UserService constructor.
     * @param UserApi $api
     */
    public function __construct(UserApi $api)
    {
        parent::__construct($api);
    }

    /**
     * @return string
     */
    function getResponseClass(): string
    {
        return Response::class;
    }
}
