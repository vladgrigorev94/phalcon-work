<?php

namespace ServiceClientPermission\Core;

use \ServiceClient\Core\Response as CoreResponse;

/**
 * Class Response
 * @package ServiceClientPermission\Core
 */
class Response extends CoreResponse
{
    /**
     * @return object|null
     */
    public function getAction(): ?array
    {
        return $this->contents['data']['action'] ?? null;
    }

    /**
     * @return string|null
     */
    public function getUnfinishedActionAuthMethod(): ?string
    {
        $action = $this->getAction();
        if (isset($action['auth_methods'])) {
            $key = array_search(false, $action['auth_methods']);

            return $key;
        }

        return null;
    }

}