<?php

namespace ServiceClientPermission\Core;

use ServiceClient\Core\Api;

/**
 * Class PermissionApi
 * @package ServiceClientPermission\Core
 */
class PermissionApi extends Api
{
    /**
     * PermissionApi constructor.
     * @param array $context
     */
    public function __construct(array $context = [])
    {
        parent::__construct($context);
        $this->baseUrl = getenv('MODULE_PERMISSION_ADDR') ?: 'http://localhost/permission/call';
    }

}