<?php

namespace ServiceClientPermission\Services;

use ServiceClient\Core\Service;
use ServiceClientPermission\Core\PermissionApi;
use ServiceClientPermission\Core\Response;

/**
 * Class PermissionService
 * @package ServiceClientPermission\Services
 */
class PermissionService extends Service
{
    const AUTH_METHOD_DEFAULT = 'default';
    const AUTH_METHOD_2FA = '2fa';
    const AUTH_METHOD_PGP = 'pgp';
    const AUTH_METHOD_PIN = 'pin';

    /**
     * PermissionService constructor.
     * @param PermissionApi $api
     */
    public function __construct(PermissionApi $api)
    {
        parent::__construct($api);
    }

    /**
     * @return string
     */
    function getResponseClass(): string
    {
        return Response::class;
    }
}