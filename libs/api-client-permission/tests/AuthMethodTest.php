<?php

namespace Tests;

use Carbon\Carbon;
use ParagonIE\Paseto\Builder;
use ParagonIE\Paseto\Keys\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;
use ParagonIE\Paseto\Purpose;
use PHPUnit\Framework\TestCase;
use ServiceClientPermission\Core\PermissionApi;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class AuthMethodTest
 * @package Tests
 */
class AuthMethodTest extends TestCase
{
    /**
     * @return Builder
     * @throws \ParagonIE\Paseto\Exception\InvalidKeyException
     * @throws \ParagonIE\Paseto\Exception\InvalidPurposeException
     * @throws \ParagonIE\Paseto\Exception\PasetoException
     */
    public function testGetTokenBuilder(): Builder
    {
        $key = SymmetricKey::fromEncodedString('tD2ONiqbfxcOLK9JLT6BHsQ2C8UR8BC2cs6t0t3tBcO');
        $builder = (new Builder)
            ->setKey($key)
            ->setVersion(new Version2)
            ->setPurpose(Purpose::local());

        $this->assertEquals(
            true,
            true
        );

        return $builder;
    }

    /**
     * @depends testGetTokenBuilder
     * @param Builder $builder
     * @return PermissionService
     */
    public function testConstructor(Builder $builder): PermissionService
    {
        $arr['id'] = 5;
        $arr['username'] = 'admin';
        $api = new PermissionApi($arr);
        $permissionService = new PermissionService($api);
        $token = $builder
            ->setExpiration(Carbon::now()->addHours(24))
            ->setIssuer('api')
            ->setClaims([]);
        $permissionService->setAuthToken($token);
        $this->assertEquals(
            true,
            true
        );

        return $permissionService;
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @throws \Exception
     */
    public function testListAuthMethods(PermissionService $permissionService)
    {
        $response = $permissionService->send('authMethod.index', [
            'user_id' => 5
        ]);

        $this->assertEquals(
            true,
            isset($response->data['auth_methods'])
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @return \ServiceClient\Core\ResponseInterface
     */
    public function testGetAuthMethod(PermissionService $permissionService)
    {
        $response = $permissionService->send('authMethod.show', [
            'user_id' => 5
        ]);

        $this->assertEquals(
            true,
            isset($response->data['auth_methods'])
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @return \ServiceClient\Core\ResponseInterface
     */
    public function testErrorGetAuthMethod(PermissionService $permissionService)
    {
        $response = $permissionService->send('authMethod.show', [
            'user_id' => 0
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @return \ServiceClient\Core\ResponseInterface
     */
    public function testSetAuthMethod(PermissionService $permissionService)
    {
        $response = $permissionService->send('authMethod.setAuthMethod', [
            'user_id' => 5,
            'auth_method' => PermissionService::AUTH_METHOD_DEFAULT
        ]);

        $this->assertEquals(
            true,
            $response->data['message'] === 'Success!'
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @return \ServiceClient\Core\ResponseInterface
     */
    public function testErrorSetAuthMethod(PermissionService $permissionService)
    {
        $response = $permissionService->send('authMethod.setAuthMethod', [
            'user_id' => 5,
            'auth_method' => 0
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @return \ServiceClient\Core\ResponseInterface
     */
    public function testDisableAuthMethod(PermissionService $permissionService)
    {
        $response = $permissionService->send('authMethod.disableAuthMethod', [
            'user_id' => 5,
            'auth_method_id' => 1
        ]);

        $this->assertEquals(
            true,
            $response->data['message'] === 'Success!'
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @return \ServiceClient\Core\ResponseInterface
     */
    public function testErrorDisableAuthMethod(PermissionService $permissionService)
    {
        $response = $permissionService->send('authMethod.disableAuthMethod', [
            'user_id' => 5,
            'auth_method_id' => 'test'
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }
}