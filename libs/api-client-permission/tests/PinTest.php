<?php

namespace Tests;

use Carbon\Carbon;
use ParagonIE\Paseto\Builder;
use ParagonIE\Paseto\Keys\SymmetricKey;
use ParagonIE\Paseto\Protocol\Version2;
use ParagonIE\Paseto\Purpose;
use ServiceClientPermission\Core\PermissionApi;
use ServiceClientPermission\Services\PermissionService;
use PHPUnit\Framework\TestCase;

/**
 * Class PinTest
 * @package Tests
 */
class PinTest extends TestCase
{
    /**
     * @return Builder
     * @throws \ParagonIE\Paseto\Exception\InvalidKeyException
     * @throws \ParagonIE\Paseto\Exception\InvalidPurposeException
     * @throws \ParagonIE\Paseto\Exception\PasetoException
     */
    public function testGetTokenBuilder(): Builder
    {
        $key = SymmetricKey::fromEncodedString('tD2ONiqbfxcOLK9JLT6BHsQ2C8UR8BC2cs6t0t3tBcO');
        $builder = (new Builder)
            ->setKey($key)
            ->setVersion(new Version2)
            ->setPurpose(Purpose::local());

        $this->assertEquals(
            true,
            true
        );

        return $builder;
    }

    /**
     * @depends testGetTokenBuilder
     * @param Builder $builder
     * @return PermissionService
     */
    public function testConstructor(Builder $builder): PermissionService
    {
        $arr['id'] = 5;
        $arr['username'] = 'admin';
        $api = new PermissionApi($arr);
        $permissionService = new PermissionService($api);
        $token = $builder
            ->setExpiration(Carbon::now()->addHours(24))
            ->setIssuer('api')
            ->setClaims([]);
        $permissionService->setAuthToken($token);
        $this->assertEquals(
            true,
            true
        );

        return $permissionService;
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @throws \Exception
     */
    public function testMakePin(PermissionService $permissionService)
    {
        $response = $permissionService->send('pin.makePin', [
            'user_id' => 5,
            'pin' => 123,
        ]);

        $this->assertEquals(
            true,
            $response->data['message'] === 'pin has done!'
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @throws \Exception
     */
    public function testErrorMakePin(PermissionService $permissionService)
    {
        $response = $permissionService->send('pin.makePin', [
            'user_id' => 0,
            'pin' => 123,
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @throws \Exception
     */
    public function testVerifyPin(PermissionService $permissionService)
    {
        $response = $permissionService->send('pin.verifyPin', [
            'user_id' => 5,
            'pin' => 123
        ]);

        $this->assertEquals(
            true,
            $response->data['message'] === 'Success!'
        );
    }

    /**
     * @depends testConstructor
     * @param PermissionService $permissionService
     * @throws \Exception
     */
    public function testErrorVerifyPin(PermissionService $permissionService)
    {
        $response = $permissionService->send('pin.verifyPin', [
            'user_id' => 5,
            'pin' => 1235
        ]);

        $this->assertEquals(
            true,
            isset($response->error)
        );
    }
}