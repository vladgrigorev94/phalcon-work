<?php

declare(strict_types=1);

namespace PhCore\Http;

use function json_decode;
use Phalcon\Http\Response as PhResponse;
use Phalcon\Mvc\Model\MessageInterface as ModelMessage;
use Phalcon\Validation\Message\Group as ValidationMessage;

class Response extends PhResponse
{
    const HTTP_OK = 1200;
    const HTTP_MOVED_PERMANENTLY = 1301;
    const HTTP_FOUND = 1302;
    const HTTP_TEMPORARY_REDIRECT = 1307;
    const HTTP_PERMANENT_REDIRECT = 1308;
    const HTTP_BAD_REQUEST = 2000;
    const HTTP_UNAUTHORIZED = 2001;
    const HTTP_FORBIDDEN = 2003;
    const HTTP_NOT_FOUND = 2004;
    const HTTP_INTERNAL_SERVER_ERROR = 3000;
    const HTTP_NOT_IMPLEMENTED = 3001;
    const HTTP_BAD_GATEWAY = 3002;

    private $codes = [
        self::HTTP_OK => 'OK',
        self::HTTP_MOVED_PERMANENTLY => 'Moved Permanently',
        self::HTTP_FOUND => 'Found',
        self::HTTP_TEMPORARY_REDIRECT => 'Temporary Redirect',
        self::HTTP_PERMANENT_REDIRECT => 'Permanent Redirect',
        self::HTTP_BAD_REQUEST => 'Bad Request',
        self::HTTP_UNAUTHORIZED => 'Unauthorized',
        self::HTTP_FORBIDDEN => 'Forbidden',
        self::HTTP_NOT_FOUND => 'Not Found',
        self::HTTP_INTERNAL_SERVER_ERROR => 'Internal Server Error',
        self::HTTP_NOT_IMPLEMENTED => 'Not Implemented',
        self::HTTP_BAD_GATEWAY => 'Bad Gateway',
    ];

    /**
     * Returns the http code description or if not found the code itself
     * @param int $code
     *
     * @return int|string
     */
    public function getHttpCodeDescription(int $code)
    {
        if (true === isset($this->codes[$code])) {
            return sprintf('%d (%s)', $code, $this->codes[$code]);
        }

        return $code;
    }

    /**
     * Send the response back
     *
     * @return PhResponse
     */
    public function send(): PhResponse
    {
        /** @var array $content */
        $content = json_decode($this->getContent(), true);
        $this->setJsonContent($content);
        $this->setStatusCode(200);

        return parent::send();
    }

    /**
     * Sets the payload code as Error
     *
     * @param string $detail
     * @param int $code
     *
     * @return Response
     */
    public function setPayloadError(string $detail = '', $code = 2000): Response
    {
        $this->setJsonContent([
            'code' => $code,
            'error' => [
                'message' => $detail
            ]
        ]);

        return $this;
    }

    /**
     * Traverses the errors collection and sets the errors in the payload
     *
     * @param ModelMessage[]|ValidationMessage $errors
     * @param int $code
     *
     * @return Response
     */
    public function setPayloadErrors($errors, $code = 2000): Response
    {
        $data = [];
        foreach ($errors as $error) {
            $data[] = [
                'message' => $error->getMessage()
            ];
        }

        $this->setJsonContent(['code' => $code, 'error' => $data]);

        return $this;
    }

    /**
     * Traverses the errors collection and sets the errors in the payload
     *
     * @param ModelMessage[]|ValidationMessage $errors
     * @param int $code
     *
     * @return Response
     */
    public function setValidationError($errors, $code = 2000): Response
    {
        $data = [];
        $data['message'] = 'Validation failed';
        $data['fields'] = [];
        foreach ($errors as $key => $error) {
            $data['fields'][$key]['detail'] = $error->getMessage();
            $data['fields'][$key]['rule'] = $error->getType();
            $data['fields'][$key]['name'] = $error->getField();
        }
        $this->setJsonContent(['code' => $code, 'error' => $data]);

        return $this;
    }

    /**
     * Sets the payload code as Success
     *
     * @param null|string|array $content The content
     * @param int $code
     *
     * @return Response
     */
    public function setPayloadSuccess($content = [], $code = 1200): Response
    {
        $data = (true === is_array($content)) ? $content : ['data' => $content];
        $data = (true === isset($data['data'])) ? $data : ['data' => $data];
        $data['code'] = $code;
        $this->setJsonContent($data);

        return $this;
    }
}
