<?php

declare(strict_types=1);

namespace PhCore\Middleware;

use ParagonIE\Paseto\Exception\PasetoException;
use ParagonIE\Paseto\Keys\SymmetricKey;
use ParagonIE\Paseto\Parser;
use ParagonIE\Paseto\ProtocolCollection;
use ParagonIE\Paseto\Rules\IssuedBy;
use ParagonIE\Paseto\Rules\NotExpired;
use PhCore\Http\Request;
use PhCore\Http\Response;
use PhCore\Traits\ResponseTrait;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use TypeError;

/**
 * Class AuthenticationMiddleware
 *
 * @package Niden\Middleware
 */
class AuthenticationMiddleware implements MiddlewareInterface
{
    use ResponseTrait;

    /**
     * @param Micro $api
     * @return bool
     * @throws PasetoException
     * @throws \ParagonIE\Paseto\Exception\InvalidVersionException
     */
    public function call(Micro $api)
    {
        /** @var Request $request */
        $request = $api->getService('request');
        /** @var Response $response */
        $response = $api->getService('response');
        $token = $this->getTokenFromRequest($request);
        if (!empty($token) && !$this->check($token)) {
            $this->halt(
                $api,
                $response::HTTP_UNAUTHORIZED,
                'Invalid Token'
            );

            return false;
        }

        return true;
    }

    /**
     * @param string $token
     * @return bool
     * @throws PasetoException
     * @throws \ParagonIE\Paseto\Exception\InvalidVersionException
     */
    private function check(string $token): bool
    {
        $parser = Parser::getLocal($this->getSharedKey(), ProtocolCollection::v2());

        $parser->addRule(new NotExpired);
        $parser->addRule(new IssuedBy(getenv('PASETO_AUTH_ISSUER')));
        try {
            $parser->parse($token);
        } catch (PasetoException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $request
     * @return string|bool
     */
    private function getTokenFromRequest($request): ?string
    {
        if ($token = $request->getHeader('Authorization')) {
            return str_replace('Bearer ', '', $token);
        }

        return null;
    }

    /**
     * @return SymmetricKey
     * @throws TypeError
     */
    private function getSharedKey()
    {
        return SymmetricKey::fromEncodedString(getenv('PASETO_AUTH_KEY'));
    }
}
