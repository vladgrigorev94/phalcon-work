<?php

declare(strict_types=1);

namespace PhCore\Traits;

use PhCore\Http\Response;
use Phalcon\Mvc\Micro;

/**
 * Trait ResponseTrait
 *
 * @package PhCore\Traits
 */
trait ResponseTrait
{
    /**
     * Halt execution after setting the message in the response
     *
     * @param Micro $api
     * @param int $status
     * @param string $message
     *
     * @return mixed
     */
    protected function halt(Micro $api, int $status, string $message)
    {
        /** @var Response $response */
        $response = $api->getService('response');
        $httpStatus = $this->getHttpFormat($status);
        $response
            ->setPayloadError($message, $status)
            ->setStatusCode($httpStatus)
            ->send();

        $api->stop();
    }

    /**
     * @param int $code
     * @return int
     */
    private function getHttpFormat(int $code): int
    {
        switch (true) {
            case $code < 2000 && $code >= 1000:
                return 200;
            case $code < 3000 && $code >= 2000:
                return 400;
            case $code >= 3000:
                return 500;
            default:
                return 200;
        }
    }

}
