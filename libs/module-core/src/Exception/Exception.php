<?php

declare(strict_types=1);

namespace PhCore\Exception;

use Phalcon\Exception as PhException;

class Exception extends PhException
{
}
