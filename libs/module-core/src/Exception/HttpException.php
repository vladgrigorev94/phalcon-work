<?php

declare(strict_types=1);

namespace PhCore\Exception;

class HttpException extends Exception
{
}
