<?php

declare(strict_types=1);

namespace PhCore\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use PhCore\Services\ValidationService;

/**
 * Class ValidationProvider
 * @package PhCore\Providers
 */
class ValidationProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'validation',
            function () use ($container) {
                return new ValidationService();
            }
        );
    }
}