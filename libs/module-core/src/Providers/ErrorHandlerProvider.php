<?php

declare(strict_types=1);

namespace PhCore\Providers;

use PhCore\Http\Response;
use function register_shutdown_function;
use function set_error_handler;
use Monolog\Logger;
use PhCore\ErrorHandler;
use Phalcon\Config;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;

class ErrorHandlerProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     *
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        /** @var Logger $logger */
        $logger  = $container->getShared('logger');
        /** @var Config $registry */
        $config  = $container->getShared('config');

        date_default_timezone_set('UTC');
        ini_set('display_errors', $config->app->display_errors);
        error_reporting(E_ALL);

        $res = $container->getShared('response');

        $handler = new ErrorHandler($logger, $config, $res);
        set_error_handler([$handler, 'handle']);
        register_shutdown_function([$handler, 'shutdown']);
        set_exception_handler(function (\Throwable $e) use ($logger, $container) {
            $logger->error($e->getMessage(), ['exception' => $e]);
            $res = $container->getShared('response');
            $message = getenv('APP_DEBUG', false) ? $e->getMessage() . ' ' .$e->getTraceAsString() : 'Error occurred. Please contact to administrator.';
            $res->setPayloadError($message, Response::HTTP_INTERNAL_SERVER_ERROR)
                ->send();
            exit();
        });
    }
}