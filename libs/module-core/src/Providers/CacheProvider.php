<?php

declare(strict_types=1);

namespace PhCore\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class CacheProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        /**
         * Cache
         * @description Phalcon - \Phalcon\Cache\Backend\Redis
         */
        $container->setShared('cache', function ()  {
            //Create a Data frontend and set a default lifetime to 1 hour
            $frontend = new \Phalcon\Cache\Frontend\Data(array(
                'lifetime' =>  getenv('REDIS_CACHE_LIFETIME'),
                'prefix' => getenv('REDIS_CACHE_PREFIX') . ".cache."
            ));

            //Create the cache passing the connection
            $cache = new \Phalcon\Cache\Backend\Redis($frontend, array(
                "host" => getenv('REDIS_HOST'),
                "port" => getenv('REDIS_PORT'),
                "persistent" => false,
                "prefix" => getenv('REDIS_CACHE_PREFIX') . ".cache.",
                "index" => 0,
            ));

            return $cache;
        });
    }
}
