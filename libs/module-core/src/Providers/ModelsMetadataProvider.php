<?php

declare(strict_types=1);

namespace PhCore\Providers;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use Phalcon\Config;

class ModelsMetadataProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'modelsMetadata',
            function () use ($container) {
                /** @var Config $registry */
                $config = $container->getShared('config');
                if (isset($config->models->metadata) && $config->models->metadata->adapter == 'Redis') {
                    return new \Phalcon\Mvc\Model\MetaData\Redis([
                        'host' => getenv('REDIS_HOST'),
                        'port' => getenv('REDIS_PORT'),
                        'persistent' => 0,
                        'prefix' => 'modelsMeta',
                        'lifetime' => getenv('MODELS_METADATA_LIFETIME'),
                        'index' => 0,
                    ]);
                } else {
                    return new \Phalcon\Mvc\Model\Metadata\Memory();
                }
            }
        );
    }
}
