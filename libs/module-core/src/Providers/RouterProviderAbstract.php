<?php

declare(strict_types=1);

namespace PhCore\Providers;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use Phalcon\Events\Manager;
use Phalcon\Mvc\Micro;
use PhCore\Exception\HttpException as Exception;
use PhCore\Http\Request;
use PhCore\Http\Response;

/**
 * Class RouterProviderAbstract
 * @package PhCore\Providers
 */
abstract class RouterProviderAbstract implements ServiceProviderInterface
{

    /**
     * {@inheritdoc}
     * @param DiInterface $container
     * @throws Exception
     */
    public function register(DiInterface $container)
    {
        /** @var Micro $application */
        $application = $container->getShared('application');

        /** @var Manager $eventsManager */
        $eventsManager = $container->getShared('eventsManager');

        $application->setEventsManager($eventsManager);

        $this->attachRoutes($application, $container);
        $this->attachMiddleware($application, $eventsManager);

    }

    /**
     * Attaches the middleware to the application
     *
     * @param Micro $application
     * @param Manager $eventsManager
     */
    private function attachMiddleware(Micro $application, Manager $eventsManager)
    {
        $middleware = $this->getMiddleware();

        /**
         * Get the events manager and attach the middleware to it
         */
        foreach ($middleware as $class => $function) {
            $eventsManager->attach('micro', new $class());
            $application->{$function}(new $class());
        }
    }

    /**
     * Attaches the routes to the application; lazy loaded
     * @param Micro $application
     * @param DiInterface $container
     * @throws Exception
     */
    private function attachRoutes(Micro $application, DiInterface $container)
    {
        $request = $application->getService('request');
        $res = $application->getService('response');
        if ($request->getContentType() !== 'application/json') {
            throw new \Exception(sprintf('Invalid content-type "%s" must be "application/json"', $request->getContentType()));
        }

        $body = $request->getJsonRawBody();
        if (empty($body)) {
            $res->setPayloadError('Received request body is blank or invalid json structure', Response::HTTP_BAD_REQUEST)
                ->send();
            exit;
        }

        if (!isset($body->params)) {
            $res->setPayloadError('There is no params field in request body.', Response::HTTP_BAD_REQUEST)
                ->send();
            exit;
        }

        $container->set('params', function () use ($request) {
            return new class($request) implements \ArrayAccess
            {
                private $body;

                private $associativeBody;

                public function __construct(Request $request)
                {
                    $this->body = ($request->getJsonRawBody())->params;
                    $this->associativeBody = ($request->getJsonRawBody(true))['params'];
                }

                public function offsetSet($offset, $value)
                {
                    if (is_null($offset)) {
                        $this->associativeBody[] = $value;
                    } else {
                        $this->associativeBody[$offset] = $value;
                    }
                }

                public function offsetExists($offset)
                {
                    return isset($this->associativeBody[$offset]);
                }

                public function offsetUnset($offset)
                {
                    unset($this->associativeBody[$offset]);
                }

                public function offsetGet($offset)
                {
                    return isset($this->associativeBody[$offset]) ? $this->associativeBody[$offset] : null;
                }

                public function __get(string $name)
                {
                    return $this->body->$name ?? '';
                }

                public function __isset(string $name): bool
                {
                    return isset($this->body->$name);
                }

                public function toArray(): array
                {
                    return $this->associativeBody;
                }
            };
        });

        @list($controller, $action) = explode('.', $body->method ?? '');
        if (empty($controller)) {
            $res->setPayloadError('Blank controller name. Request format: {method: controller.action}', Response::HTTP_BAD_REQUEST)
                ->send();
            exit;
        }
        if (empty($action)) {
            $res->setPayloadError('Blank action name. Request format: {method: controller.action}', Response::HTTP_BAD_REQUEST)
                ->send();
            exit;
        }
        $controller = ucfirst($controller);
        $service = explode('/', $request->getURI())[1];
        if (!$service) {
            $res->setPayloadError('Service not defined', Response::HTTP_BAD_REQUEST)
                ->send();
            exit;
        }
        $serviceControllerName = sprintf('\\Ph%s\\Controllers\\%sController', ucfirst($service), $controller);
        if (!class_exists($serviceControllerName)) {
            $message = sprintf('Controller "%s" not found. Namespace: %s', $controller, $serviceControllerName);
            $res->setPayloadError($message, Response::HTTP_NOT_FOUND)
                ->send();
            exit;
        }
        $controllerClass = new $serviceControllerName();
        if (!method_exists($controllerClass, $action)) {
            $message = sprintf('Action "%s" not found in "%s" controller', $action, $serviceControllerName);
            $res->setPayloadError($message, Response::HTTP_NOT_FOUND)
                ->send();
            exit;
        }

        $routeCollection = new \Phalcon\Mvc\Micro\Collection();

        $routeCollection
            ->setPrefix('')
            ->setHandler($serviceControllerName)
            ->setLazy(true);

        $routeCollection->post("/{$service}/call", $action, $controller . $action);

        $application->mount($routeCollection);
    }

    /**
     * Returns the array for the middleware with the action to attach
     *
     * @return array
     */
    abstract function getMiddleware(): array;
}
