<?php

declare(strict_types=1);

namespace PhCore\Providers;


use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;

class LoggerProvider implements ServiceProviderInterface
{
    /**
     * Registers the logger component
     *
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'logger',
            function () use($container) {
                $config  = $container->getShared('config');
                $logger = new \Monolog\Logger($config->logger->name);
                $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($config->logger->path, $config->logger->level));
                return $logger;
            }
        );
    }
}
