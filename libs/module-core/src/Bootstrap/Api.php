<?php

declare(strict_types=1);

namespace PhCore\Bootstrap;

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

/**
 * Class Api
 *
 * @package PhCore\Bootstrap
 *
 * @property Micro $application
 */
class Api extends AbstractBootstrap
{
    /**
     * Run the application
     *
     * @return mixed
     */
    public function run()
    {
        return $this->application->handle();
    }

    /**
     * @return mixed
     */
    public function setup()
    {
        //we use a FactoryDefault class because it automatically
        //registers all the services provided by the framework by default
        $this->container = new FactoryDefault();
        $this->providers = require BASE_DIR . '/config/providers.php';

        parent::setup();
    }
}
