<?php

declare(strict_types=1);

namespace PhCore\Services;

use Phalcon\Validation;

/**
 * Class ValidationService
 * @package PhCore\Services
 */
class ValidationService
{
    /**
     * @var object
     */
    private $messages;

    /**
     * @param array|object $params
     * @param Validation $validation
     * @return bool
     */
    public function run($params, Validation $validation): bool
    {
        $messages = $validation->validate($params);
        if (count($messages)) {
            $this->messages = $messages;

            return false;
        }

        return true;
    }

    /**
     * @return object
     */
    public function getMessages(): object
    {
        return $this->messages;
    }
}

