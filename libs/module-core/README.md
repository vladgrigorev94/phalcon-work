# Docker Phalcon development environment

## Dependencies

- [Docker Engine ~> 18.09.3](https://docs.docker.com/installation/)
- [Docker compose ~> 1.23.2](https://docs.docker.com/compose/install/)

## Usage

    docker-compose up -d --build

    visit http://localhost:8000/api/v1/users

## Generate Docs

`doxygen Doxyfile` (generated docs folder)