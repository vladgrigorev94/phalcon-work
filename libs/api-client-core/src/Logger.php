<?php

namespace ServiceClient;

class Logger
{
    protected $writeFile = [];

    public function __construct(string $writeFile)
    {
        $this->writeFile = $writeFile;
        $this->time = $this->getTime();
    }

    protected function getTime(): float {
        return round(microtime(true) * 1000);
    }

    public function write(string $url, array $request, array $headers, array $context, string $response): bool {
        if (empty($this->writeFile)) {
            return false;
        }
        $request = json_encode($request, JSON_UNESCAPED_SLASHES);
        $context = json_encode($context, JSON_UNESCAPED_SLASHES);
        $headers = json_encode($headers, JSON_UNESCAPED_SLASHES);
        $backTrace = debug_backtrace();
        unset($backTrace[0]);
        $backTrace = array_values($backTrace);
        $backTrace = json_encode($backTrace, JSON_UNESCAPED_SLASHES);

        $writeData = sprintf(
            "App: %s\nDate: %s\nExec time: %sµ\nURL: %s\nRequest: %s\nHeaders: %s\nContext: %s\nResponse: %s\nDebug: %s",
            APP_NAME ?? 'unknown',
            date(DATE_RFC822),
            $this->getTime() - $this->time,
            $url,
            $request,
            $headers,
            $context,
            $response,
            $backTrace
        );
        $writable = file_put_contents($this->writeFile, $writeData . "\n---\n", FILE_APPEND);
        return (bool) $writable;
    }

}