<?php

namespace ServiceClient\Core;

use GuzzleHttp\Client;
use ServiceClient\Logger;

/**
 * Class Service
 * @package ServiceClient\Core
 */
abstract class Service
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $authToken = '';

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Controller constructor.
     * @param Api $client
     */
    public function __construct(Api $client)
    {
        $this->client = $client;
        $this->logger = new Logger(getenv('LOGGER_FILE_PATH') ?: '/var/www/logs/api.log');
    }

    /**
     * @return Api
     */
    public function getClient(): Api
    {
        return $this->client;
    }

    /**
     * @param string $token
     */
    public function setAuthToken(string $token)
    {
        $this->authToken = $token;
    }

    /**
     * @return string
     */
    abstract public function getResponseClass(): string;

    /**
     * @param $endpoint
     * @param array $options
     * @return Response
     * @throws \Exception
     */
    public function request($endpoint, $options = []): ResponseInterface
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        if (!empty($this->authToken)) {
            $headers['Authorization'] = 'Bearer ' . $this->authToken;
        }

        $client = new Client([
            'headers' => $headers,
            'defaults' => [
                'exceptions' => false
            ]
        ]);

        $request = [
            'json' => [
                'method' => $endpoint,
                'params' => $options
            ],
            'http_errors' => false,
        ];


        $error = '';
        $requestUrl = $this->client->getBaseUrl();
        $context = $this->client->getContext();
        try {
            $response = $client->post($requestUrl, $request);
        } catch (\Exception $e) {
            $error = 'Response error: ' . $e->getMessage();
        }
        $responseBody = $response->getBody()->getContents();
        $hasError = empty($error) === false;
        $this->logger->write($requestUrl, $request, $headers, $context, $error ?: $responseBody);

        if ($hasError) {
            throw new \Exception('Connection error: ' . $response, 500);
        }
        $responseClass = $this->getResponseClass();
        $response = new $responseClass($responseBody);
        // @TODO exception
        return $response;
    }

    /**
     * @param string|null $endpoint
     * @param array $params
     * @return ResponseInterface
     * @throws \Exception
     */
    public function send(string $endpoint = null, array $params = []): ResponseInterface
    {
        if(!$endpoint){
            throw  new \Exception('Endpoint and params mandatory in request to user microservice.');
        }

        return $this->request($endpoint, $params);
    }
}