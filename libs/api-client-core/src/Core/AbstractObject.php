<?php

namespace ServiceClient\Core;

/**
 * Class AbstractObject
 * @package ServiceClient\Core
 */
abstract class AbstractObject
{
    /**
     * @var array
     */
    protected $params = [];

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->initParams();
    }

    /**
     * @return mixed
     */
    abstract public function initParams();

    /**
     * @param string $name
     * @param $value
     * @throws \Exception
     */
    public function __set(string $name, $value)
    {
        if (in_array($name, $this->params)) {
            $this->params[$name] = $value;
        } else {
            throw new \Exception('Set unknown property ' . $name);
        }
    }

    /**
     * @param string $name
     * @return mixed|string
     * @throws \Exception
     */
    public function __get(string $name)
    {
        if (in_array($name, $this->params)) {
            //get_from_db
            return $this->params[$name] ?? '';
        } else {
            throw new \Exception('Get unknown property ' . $name);
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset(string $name): bool
    {
        return isset($this->params[$name]);
    }

    /**
     * @param string $name
     */
    public function __unset(string $name)
    {
        unset($this->params[$name]);
    }

    /**
     * @param array $data
     */
    public function assign(array $data): void
    {
        foreach ($data as $key => $value) {
            if (!empty($value)) {
                $this->$key = $value;
            }
        }
    }
}