<?php

namespace ServiceClient\Core;

/**
 * Class Response
 * @package ServiceClient\Core
 */
class Response implements ResponseInterface
{
    /**
     * @var mixed
     */
    protected $contents;

    /**
     * Response constructor.
     * @param string $contents
     */
    public function __construct(string $contents)
    {
        $this->contents = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @return array
     */
    public function toArray(): ?array
    {
        return $this->contents;
    }

    /**
     * @return object
     */
    public function toObject(): object
    {
        return $this->arrayToObject($this->contents);
    }

    /**
     * @param $d
     * @return object
     */
    public function arrayToObject($d)
    {
        return is_array($d) ? (object)array_map(__METHOD__, $d) : $d;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return json_encode($this->contents);
    }

    /**
     * @param string $name
     * @return null
     */
    public function __get(string $name)
    {
        return $this->contents[$name] ?? null;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function __isset(string $field): bool
    {
        return isset($this->contents[$field]);
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return isset($this->contents['error']);
    }

    /**
     * @return array
     */
    public function getError(): array
    {
        return $this->contents['error'];
    }

    /**
     * @return string
     */
    public function gerErrorMessage(): string
    {
        return $this->contents['error']['message'];
    }

}