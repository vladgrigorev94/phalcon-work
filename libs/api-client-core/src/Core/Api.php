<?php

namespace ServiceClient\Core;

/**
 * Class Api
 * @package ServiceClient\Services
 */
class Api
{
    /**
     * @var array
     */
    private $context;

    /**
     * @var
     */
    protected $baseUrl;

    /**
     * Api constructor.
     * @param array $context
     */
    public function __construct(array $context = [])
    {
        $this->context = $context;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

}