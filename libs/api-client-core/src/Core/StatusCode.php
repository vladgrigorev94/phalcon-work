<?php

namespace ServiceClient\Core;

/**
 * Interface StatusCode
 * @package ServiceClient\Core
 */
interface StatusCode
{
    const HTTP_OK = 1200;
    const HTTP_MOVED_PERMANENTLY = 1301;
    const HTTP_FOUND = 1302;
    const HTTP_TEMPORARY_REDIRECT = 1307;
    const HTTP_PERMANENT_REDIRECT = 1308;
    const HTTP_BAD_REQUEST = 2000;
    const HTTP_UNAUTHORIZED = 2001;
    const HTTP_FORBIDDEN = 2003;
    const HTTP_NOT_FOUND = 2004;
    const HTTP_INTERNAL_SERVER_ERROR = 3000;
    const HTTP_NOT_IMPLEMENTED = 3001;
    const HTTP_BAD_GATEWAY = 3002;

}