<?php

namespace ServiceClientTest;

use PHPUnit\Framework\TestCase;
use ServiceClient\Core\Response;

class ResponseTest extends TestCase
{
    public function testResponseToArray()
    {
        $response = new Response('{}');

        $this->assertInternalType(
            'array',
            $response->toArray()
        );
    }

    public function testResponseToObject()
    {
        $response = new Response('{}');

        $this->assertInternalType(
            'object',
            $response->toObject()
        );
    }
}