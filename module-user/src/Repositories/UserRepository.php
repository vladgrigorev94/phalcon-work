<?php

declare(strict_types=1);

namespace PhUser\Repositories;

use Phalcon\Di;
use Phalcon\Mvc\ModelInterface;
use PhCore\Exception\ModelException;
use PhUser\Exceptions\UserNotFoundException;
use PhUser\User\UserModel;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class UserRepository
 * @package PhUser\Repositories
 */
class UserRepository
{
    /**
     * @param int $id
     * @return ModelInterface|null
     * @throws UserNotFoundException
     */
    public static function retrieveById(int $id): ?ModelInterface
    {
        $obj = UserModel::query()
            ->where('id = :id:')
            ->bind(['id' => $id])
            ->execute()
            ->getFirst();

        if (!$id || !$obj) {
            throw new UserNotFoundException('model not found');
        }

        return $obj;
    }

    /**
     * @param array $params
     * @throws ModelException
     */
    public static function userRegistration(array $params): void
    {
        $service = Di::getDefault();
        $user = new UserModel();
        $security = $service->get('security');
        $user->set('username', $params['username'])->set('password', $security->hash($params['password']))->save();
        $permission = $service->get('permission');
        //getting $user->id like string by unknown reason
        $res = $permission->setRole((int)$user->id, 'User');
        if ($res->hasError()) {
            throw new \Exception($res->getError()['message']);
        }
    }

    /**
     * @param array $params
     * @return string
     * @throws \Exception
     */
    public static function userLogin(array $params): string
    {
        $service = Di::getDefault();
        $auth = $service->get('auth');
        $token = $auth->attempt($params);
        if (!$token) {
            throw new \Exception('invalid login or password');
        }

        return $token;
    }

    /**
     * @param string $token
     * @return array
     * @throws \Exception
     */
    public static function generateActionForUserLogin(string $token): array
    {
        $service = Di::getDefault();
        $auth = $service->get('auth');
        $permission = $service->get('permission');
        $permission->setToken($token);

        $authMethods = [PermissionService::AUTH_METHOD_2FA, PermissionService::AUTH_METHOD_PGP];
        $res = $permission->generateAction($auth->user()->id, 'doLogin', $authMethods);
        if ($res->hasError()) {
            throw new \Exception($res->getError()['message']);
        }
        $authSessionToken = $res->data['auth_session_token'];
        $action = $res->getAction();
        $authMethod = $res->getUnfinishedActionAuthMethod();
        if (empty($authMethod)) {
            return [
                'auth_session_token' => $authSessionToken
            ];
        }

        return [
            'action' => $action,
            'auth_session_token' => $authSessionToken
        ];
    }

    /**
     * @param int $id
     * @param array $params
     * @return bool
     * @throws UserNotFoundException
     */
    public static function updateById(int $id, array $params): bool
    {
        $user = UserRepository::retrieveById($id);
        $user->assign($params);

        return $user->save();
    }

    /**
     * @param int $id
     * @return array
     * @throws UserNotFoundException
     */
    public static function getUserProfileById(int $id): array
    {
        $service = Di::getDefault();
        $permission = $service->get('permission');
        $user = UserRepository::retrieveById($id);
        $res = $permission->getRole($id);
        if ($res->hasError()) {
            throw new UserNotFoundException($res->getError()['message']);
        }
        $role = $res->data['role'];
        $res = $permission->getAuthMethod($id);
        if ($res->hasError()) {
            throw new UserNotFoundException($res->getError()['message']);
        }
        $permission = $res->data['auth_methods'];

        return array_merge($user->toArray(), [
            'role' => $role,
            'permission' => $permission
        ]);
    }
}