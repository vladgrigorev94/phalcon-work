<?php

namespace PhUser\Controllers;

use Phalcon\Mvc\Controller;
use PhUser\Repositories\UserRepository;
use PhUser\Validation\ProfileValidation;

/**
 * Class UserController
 * @package PhUser\Controllers
 */
class UserController extends Controller
{
    /**
     * @return mixed
     * @throws \PhUser\Exceptions\UserNotFoundException
     */
    public function profile()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new ProfileValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $this->auth->check();
        $data = UserRepository::getUserProfileById($params->user_id);

        return $this
            ->response
            ->setPayloadSuccess($data)
            ->setStatusCode(200);
    }

    /**
     * @return mixed
     * @throws \PhUser\Exceptions\UserNotFoundException
     */
    public function update()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new ProfileValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        UserRepository::updateById($params['user_id'], $params->toArray());

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'user update!']);
    }
}