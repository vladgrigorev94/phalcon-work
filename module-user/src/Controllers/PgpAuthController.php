<?php

namespace PhUser\Controllers;

use Phalcon\Mvc\Controller;
use PhPermission\Services\PgpService;
use PhUser\Validation\UserPGPLoginValidation;
use ServiceClientPermission\Objects\AuthMethod;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class PgpAuthController
 * @package PhUser\Controllers
 */
class PgpAuthController extends Controller
{
    /**
     * @return mixed
     */
    public function doPGP()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new UserPGPLoginValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        try {
            $action = $this->pgpAuth->run(
                $params['auth_session_token'],
                $params['decoded_string'],
                $params['action_name'],
                PermissionService::AUTH_METHOD_PGP
            );
        } catch (\Exception $e) {
            return $this->response->setPayloadError($e->getMessage());
        }

        if (!empty($action)) {
            return $this
                ->response
                ->setPayloadSuccess([
                    'auth_session_token' => $params['auth_session_token'],
                    'action' => $action
                ]);
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'message' => 'success!'
            ]);
    }
}