<?php

namespace PhUser\Controllers;

use Phalcon\Mvc\Controller;
use PhUser\Repositories\UserRepository;
use PhUser\Validation\UserRegistrationValidation;
use PhUser\Validation\UserLoginValidation;
use PhUser\Validation\NewPasswordValidation;
use Phalcon\Validation\Validator\Callback as CallbackValidator;
use PhUser\Validation\VerifyTokenValidation;

/**
 * Class AuthController
 * @package PhUser\Controllers
 */
class AuthController extends Controller
{

    /**
     * @return mixed
     */
    public function doCheck()
    {
        if(!$this->auth->check()){
            return $this
                ->response
                ->setPayloadError('access denied');
        }
        return $this
            ->response
            ->setPayloadSuccess(['message' => 'ok']);
    }

    /**
     * @return mixed
     * @throws \PhCore\Exception\ModelException
     */
    public function doRegistration()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new UserRegistrationValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        UserRepository::userRegistration($params->toArray());

        return $this
            ->response
            ->setPayloadSuccess(
                [
                   'message' => 'You are registered now. Please login'
                ]
            );
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function doLogin()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new UserLoginValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        try {
            $token = UserRepository::userLogin($params->toArray());
        } catch (\Exception $e) {
            return $this
                ->response
                ->setPayloadError($e->getMessage());
        }
        $data = UserRepository::generateActionForUserLogin($token);

        if (isset($data['action'])) {
            return $this
                ->response
                ->setPayloadSuccess([
                    'auth_session_token' => $data['auth_session_token'],
                    'action' => $data['action'],
                    'user_id' => $this->auth->user()->id
                ]);
        } else {
            $res = $this->permission->setActionComplete(
                $this->auth->user()->id,
                $data['auth_session_token'],
                'doLogin'
            );
            if ($res->hasError()) {
                return $this
                    ->response
                    ->setPayloadError($res->getError()['message']);
            }
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'id' => $this->auth->user()->id,
                'username' => $this->auth->user()->username,
                'token' => $token
            ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function verifyToken()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new VerifyTokenValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $user = $this->mainAuth->verify($params['auth_session_token']);
        $this->auth->setUser($user);
        $token = $this->auth->generateTokenForUser();

        $res = $this->permission->setActionComplete(
            $params['user_id'],
            $params['auth_session_token'],
            'doLogin'
        );
        if ($res->hasError()) {
            return $this
                ->response
                ->setPayloadError($res->getError()['message']);
        }
        if(!$res->data['complete']) {
            return $this
                ->response
                ->setPayloadError('please finish other auths');
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'id' => $this->auth->user()->id,
                'username' => $this->auth->user()->username,
                'token' => $token
            ]);
    }

    /**
     * @return mixed
     * @throws \PhUser\Exceptions\UserNotFoundException
     */
    public function changePassword()
    {
        $params = $this->params;
        $validator = new NewPasswordValidation();
        if (!$this->validation->run($params->toArray(), $validator)) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $user = UserRepository::retrieveById($params->user_id);
        $validator->add(
            ['password'],
            new CallbackValidator(
                [
                    'message' => 'Entered password should be equal current password',
                    'callback' => function ($data) use ($user) {
                        if ($this->security->checkHash($data['password'], $user->password)) {
                            return true;
                        }
                        return false;
                    }
                ]
            )
        );
        if (!$this->validation->run($params->toArray(), $validator)) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $user->password = $this->security->hash($params['new_password']);
        $user->save();

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Password has changed']);
    }
}
