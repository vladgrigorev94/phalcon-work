<?php

namespace PhUser\Controllers;

use Phalcon\Mvc\Controller;
use PhUser\Validation\UserPinLoginValidation;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class PinAuthController
 * @package PhUser\Controllers
 */
class PinAuthController extends Controller
{
    /**
     * @return mixed
     */
    public function doPin()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new UserPinLoginValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $action = $this->pinAuth->run(
            $params['auth_session_token'],
            $params['pin'],
            $params['action_name'],
            PermissionService::AUTH_METHOD_PIN
        );
        if (!empty($action)) {
            return $this
                ->response
                ->setPayloadSuccess([
                    'auth_session_token' => $params['auth_session_token'],
                    'action' => $action
                ]);
        }

        return $this
            ->response
            ->setPayloadSuccess([
                'message' => 'success!'
            ]);
    }
}