<?php

namespace PhUser\Exceptions;

/**
 * Class UserNotFoundException
 * @package PhUser\Exceptions
 */
class UserNotFoundException extends \Exception
{
}

