<?php

namespace PhUser\Components\AuthMethods;

use Phalcon\Mvc\ModelInterface;

/**
 * Class PgpAuthComponent
 * @package PhUser\Components
 */
class PgpAuthComponent extends AbstractAuthComponent
{
    /**
     * @param string $sessionToken
     * @param string|null $str
     * @return ModelInterface
     * @throws \Exception
     */
    public function verify(string $sessionToken, string $str = null): ModelInterface
    {
        $user = $this->getUserBySessionToken($sessionToken);
        if(!$this->provider->validatePGP($user, $str)) {
            throw new \Exception('invalid decode string');
        }

        return $user;
    }
}

