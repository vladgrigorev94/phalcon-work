<?php

namespace PhUser\Components\AuthMethods;

use Phalcon\Mvc\ModelInterface;

/**
 * Class MainAuthComponent
 * @package PhUser\Components\AuthMethods
 */
class MainAuthComponent extends AbstractAuthComponent
{
    /**
     * @param string $sessionToken
     * @param string|null $str
     * @return ModelInterface
     * @throws \Exception
     */
    public function verify(string $sessionToken, string $str = null): ModelInterface
    {
        $user = $this->getUserBySessionToken($sessionToken);

        return $user;
    }

}

