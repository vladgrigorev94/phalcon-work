<?php

namespace PhUser\Components\AuthMethods;

use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\User\Component;
use PhUser\Guard\AuthGuard;
use PhUser\Guard\Guard;
use PhUser\User\IUserProvider;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class AbstractAuthComponent
 * @package PhUser\Components
 */
abstract class AbstractAuthComponent extends Component
{
    /**
     * @var Guard
     */
    protected $authGuard;

    /**
     * @var IUserProvider
     */
    protected $provider;

    public function __construct(AuthGuard $authGuard, IUserProvider $provider)
    {
        $this->authGuard = $authGuard;
        $this->provider = $provider;
    }

    /**
     * @param string $sessionToken
     * @param string|null $str
     * @return ModelInterface
     */
    abstract public function verify(string $sessionToken, string $str = null): ModelInterface;

    /**
     * @param $authSessionToken
     * @param $code
     * @param $actionName
     * @param string $authMethod
     * @return array|null
     * @throws \Exception
     */
    public function run($authSessionToken, $code, $actionName, $authMethod = PermissionService::AUTH_METHOD_2FA): array
    {
        $user = $this->verify($authSessionToken, $code);
        if (!$user) {
            throw new \Exception('corrupt login session');
        }
        $res = $this->permission->completeAuthMethod($user->id, $actionName, $authMethod, $authSessionToken);
        if ($res->hasError()) {
            throw new \Exception($res->getError()['message']);
        }
        $action = $res->getAction();

        return $action;
    }

    /**
     * @param string $sessionToken
     * @return ModelInterface
     * @throws \Exception
     */
    protected function getUserBySessionToken(string $sessionToken): ModelInterface
    {
        $res = $this->permission->getUserIdByAuthSessionToken($sessionToken);

        if ($res->hasError()) {
            throw new \Exception($res->getError()['message']);
        }
        $userId = $res->data['user_id'] ?? '';
        $user = $this->provider->retrieveById($userId);

        if (empty($user)) {
            throw new \Exception('user not found');
        }

        return $user;
    }
}

