<?php

namespace PhUser\Components\AuthMethods;

use Phalcon\Mvc\ModelInterface;

/**
 * Class PinAuthComponent
 * @package PhUser\Components
 */
class PinAuthComponent extends AbstractAuthComponent
{
    /**
     * @param string $sessionToken
     * @param string|null $str
     * @return ModelInterface
     * @throws \Exception
     */
    public function verify(string $sessionToken, string $str = null): ModelInterface
    {
        $user = $this->getUserBySessionToken($sessionToken);
        if (!$this->provider->validatePin($user, $str)) {
            throw new \Exception('invalid pin string');
        }

        return $user;
    }
}

