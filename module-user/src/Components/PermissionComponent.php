<?php

namespace PhUser\Components;

use Phalcon\Mvc\User\Component;
use ServiceClient\Core\ResponseInterface;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class PermissionComponent
 * @package PhUser\Components
 */
class PermissionComponent extends Component
{

    /**
     * @var PermissionService
     */
    private $service;

    /**
     * PermissionComponent constructor.
     * @param PermissionService $service
     */
    public function __construct(PermissionService $service)
    {
        $this->service = $service;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->service->setAuthToken($token);
    }

    /**
     * @param int $userId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getAuthMethod(int $userId): ResponseInterface
    {
        return $this->service->send('authMethod.show', ['user_id' => $userId]);
    }

    /**
     * @param int $userId
     * @param string $authMethodId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function setAuthMethod(int $userId, string $authMethodId): ResponseInterface
    {
        return $this->service->send('authMethod.setAuthMethod', [
            'user_id' => $userId,
            'auth_method_id' => $authMethodId,
        ]);
    }

    /**
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getRoles(): ResponseInterface
    {
        return $this->service->send('role.index');
    }

    /**
     * @param int $userId
     * @param string $role
     * @return ResponseInterface
     * @throws \Exception
     */
    public function setRole(int $userId, string $role): ResponseInterface
    {
        $roles = $this->getRoles();
        if ($roles->hasError()) {
            throw new \Exception($roles->getError());
        }
        $roles = $roles->toArray();
        $ids = array_column($roles['data']['roles'], 'id');
        $names = array_column($roles['data']['roles'], 'name');
        $roles = array_combine($names, $ids);
        if (!isset($roles[$role])) {
            throw new \Exception('Role ' . $role . ' not found');
        }
        $roleId = $roles[$role];

        return $this->service->send('role.update', [
            'user_id' => $userId,
            'role_id' => $roleId
        ]);
    }

    /**
     * @param int $userId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getRole(int $userId): ResponseInterface
    {
        return $this->service->send('role.show', [
            'user_id' => $userId
        ]);
    }

    /**
     * @param int $userId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getEncryptHash(int $userId): ResponseInterface
    {
        return $this->service->send('pgp.getEncryptHash', [
            'user_id' => $userId
        ]);
    }

    /**
     * @param int $userId
     * @param string $decryptedString
     * @return ResponseInterface
     * @throws \Exception
     */
    public function verifyDecryptedHash(int $userId, string $decryptedString): ResponseInterface
    {
        return $this->service->send('pgp.verifyDecryptHash', [
            'user_id' => $userId,
            'decrypt_hash' => $decryptedString
        ]);
    }

    /**
     * @param int $userId
     * @param string $pin
     * @return ResponseInterface
     * @throws \Exception
     */
    public function verifyPin(int $userId, string $pin): ResponseInterface
    {
        return $this->service->send('pin.verifyPin', [
            'user_id' => $userId,
            'pin' => $pin
        ]);
    }

    /**
     * @param int $userId
     * @param string $actionName
     * @param array $authMethods
     * @return ResponseInterface
     * @throws \Exception
     */
    public function generateAction(int $userId, string $actionName, array $authMethods): ResponseInterface
    {
        return $this->service->send('action.generate', [
            'user_id' => $userId,
            'action_name' => $actionName,
            'auth_methods' => $authMethods
        ]);
    }

    /**
     * @param int $userId
     * @param string $actionName
     * @param string $authMethod
     * @param string $authSessionToken
     * @return ResponseInterface
     * @throws \Exception
     */
    public function completeAuthMethod(int $userId, string $actionName, string $authMethod, string $authSessionToken): ResponseInterface
    {
        return $this->service->send('action.completeAuthMethod', [
            'user_id' => $userId,
            'action_name' => $actionName,
            'auth_method' => $authMethod,
            'auth_session_token' => $authSessionToken
        ]);
    }

    /**
     * @param int $userId
     * @param string $authSessionToken
     * @param string $actionName
     * @return ResponseInterface
     * @throws \Exception
     */
    public function setActionComplete(int $userId, string $authSessionToken, string $actionName): ResponseInterface
    {
        return $this->service->send('action.setActionComplete', [
            'user_id' => $userId,
            'action_name' => $actionName,
            'auth_session_token' => $authSessionToken
        ]);
    }

    /**
     * @param string $authSessionToken
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getUserIdByAuthSessionToken(string $authSessionToken): ResponseInterface
    {
        return $this->service->send('action.getUserIdByAuthSessionToken', ['auth_session_token' => $authSessionToken]);
    }

    /**
     * @param int $userId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getGoogleAuthSecret(int $userId): ResponseInterface
    {
        return $this->service->send('multifactor.getGoogleAuthSecret', ['user_id' => $userId]);
    }

    /**
     * @param int $userId
     * @param string $googleAuthSecret
     * @return ResponseInterface
     * @throws \Exception
     */
    public function setGoogleAuthSecret(int $userId, string $googleAuthSecret): ResponseInterface
    {
        return $this->service->send('multifactor.setGoogleAuthSecret', [
            'user_id' => $userId,
            'google_auth_secret' => $googleAuthSecret
        ]);
    }
}

