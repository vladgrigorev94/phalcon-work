<?php

namespace PhUser\User;

use Phalcon\Mvc\ModelInterface;

interface IUserProvider
{
    /**
     * Retrieve a user by their unique identifier.
     *
     * @param $identifier
     * @return ModelInterface|null
     */
    public function retrieveById($identifier):? ModelInterface;

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \stdClass
     */
    public function retrieveByCredentials(array $credentials);

    /**
     * Validate a user against the given credentials.
     *
     * @param ModelInterface $user
     * @param array $credentials
     * @return mixed
     */
    public function validateCredentials(ModelInterface $user, array $credentials);

    /**
     * @param ModelInterface $user
     * @param string $pin
     * @return mixed
     */
    public function validatePin(ModelInterface $user, string $pin);

    /**
     * @param ModelInterface $user
     * @param string $decodedString
     * @return mixed
     */
    public function validatePGP(ModelInterface $user, string $decodedString);
}