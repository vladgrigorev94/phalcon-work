<?php

namespace PhUser\User;

use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\User\Plugin;
use PhUser\User\UserModel as Users;

/**
 * Class UserProvider
 * @package PhUser\User
 */
class UserProvider extends Plugin implements IUserProvider
{

    /**
     * @param mixed $id
     * @return ModelInterface|null
     */
    public function retrieveById($id):? ModelInterface
    {
        /**
         * из-за cache не работает почему-то
         */
//        $user = Users::query()->where('id = :id:')
//            ->bind(['id' => $id])->cache(['key' => 'user_id_' . (int)$id])
//            ->execute()->getFirst();

        $user = Users::query()
            ->where('id = :id:')
            ->bind(['id' => $id])
            ->execute()->getFirst();

        if (!$id || !$user) {
            return null;
        }

        return $user;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return Users|bool
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials) ||
            (count($credentials) === 1 &&
                array_key_exists('password', $credentials))
        ) {
            return false;
        }

        $user = Users::query()->where('username = :username:')
            ->bind(['username' => $credentials['username']])
            ->execute()->getFirst();

        if (!$user) {
            return false;
        }

        return $user;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param ModelInterface $user
     * @param array $credentials
     * @return bool|mixed
     */
    public function validateCredentials(ModelInterface $user, array $credentials)
    {
        return $this->security->checkHash(trim($credentials['password']), $user->password);
    }

    /**
     * @param ModelInterface $user
     * @param string $pin
     * @return bool|mixed
     */
    public function validatePin(ModelInterface $user, string $pin)
    {
        $res = $this->pinAuth->verify($user->id, $pin);

        return !$res->hasError();
    }

    /**
     * @param ModelInterface $user
     * @param string $decodedString
     * @return bool
     */
    public function validatePGP(ModelInterface $user, string $decodedString): bool
    {
        $res = $this->permission->verifyDecryptedHash($user->id, $decodedString);

        return !$res->hasError();
    }
}