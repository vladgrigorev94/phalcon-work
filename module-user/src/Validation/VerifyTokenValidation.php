<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

/**
 * Class VerifyTokenValidation
 * @package PhUser\Validation
 */
class VerifyTokenValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfSessionToken = new PresenceOf(
            [
                'message' => 'Have no auth_session_token. Please login again or go to our callcenter',
            ]
        );
        $this->setFilters('auth_session_token', Filter::FILTER_STRIPTAGS);
        $this->add('auth_session_token', $presenceOfSessionToken);
    }
}
