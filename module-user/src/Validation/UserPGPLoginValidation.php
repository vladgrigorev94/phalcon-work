<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class UserPGPLoginValidation extends Validation
{
    public function initialize()
    {
        $presenceOfSessionToken = new PresenceOf(
            [
                'message' => 'Have no session token. Please login again or go to our callcenter',
            ]
        );
        $this->setFilters('auth_session_token', Filter::FILTER_STRIPTAGS);
        $this->add('auth_session_token', $presenceOfSessionToken);

        $presenceOfPrivateKeyCode = new PresenceOf(
            [
                'message' => 'decoded_string is required',
            ]
        );
        $this->setFilters('decoded_string', Filter::FILTER_STRIPTAGS);
        $this->add('decoded_string', $presenceOfPrivateKeyCode);

        $presenceOfActionName = new PresenceOf(
            [
                'message' => 'Field action_name is required',
            ]
        );
        $this->setFilters('action_name', Filter::FILTER_STRIPTAGS);
        $this->add('action_name', $presenceOfActionName);
    }
}
