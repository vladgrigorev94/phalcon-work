<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class ProfileValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);
    }
}
