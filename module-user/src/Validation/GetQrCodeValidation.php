<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Callback as CallbackValidator;
use PhUser\User\UserModel;

/**
 * Class GetQrCodeValidation
 * @package PhUser\Validation
 */
class GetQrCodeValidation extends Validation
{
    /**
     * @return null
     */
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $this->add(
            ['user_id'],
            new CallbackValidator(
                [
                    'message' => 'There is no user in db',
                    'callback' => function ($data) {
                        return !empty(UserModel::query()
                            ->where('id = :id:')
                            ->bind(['id' => $data['user_id']])
                            ->execute()
                            ->getFirst());
                    }
                ]
            )
        );
    }
}
