<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Uniqueness;
use PhUser\User\UserModel;

class UserRegistrationValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUsername = new PresenceOf(
            [
                'message' => 'The username is required',
            ]
        );

        $uniqueOfUsername = new Uniqueness(
            [
                "model"   => new UserModel(),
                'message' => 'The username should be unique',
            ]
        );

        $presenceOfPassword = new PresenceOf(
            [
                'message' => 'The password is required',
            ]
        );
        $this->setFilters('username', Filter::FILTER_STRIPTAGS);
        $this->add('username', $presenceOfUsername);
        $this->add('username', $uniqueOfUsername);
        $this->add('password', $presenceOfPassword);
    }
}
