<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class UserLoginValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUsername = new PresenceOf(
            [
                'message' => 'The username is required',
            ]
        );
        $presenceOfPassword = new PresenceOf(
            [
                'message' => 'The password is required',
            ]
        );
        $this->setFilters('username', Filter::FILTER_STRIPTAGS);
        $this->add('username', $presenceOfUsername);
        $this->add('password', $presenceOfPassword);
    }
}
