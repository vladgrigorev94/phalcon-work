<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Alnum;

class NewPasswordValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfPassword = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field password is required',
            ]
        );
        $this->add('password', $presenceOfPassword);

        $presenceOfNewPassword = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field new_password is required',
            ]
        );
        $this->add('new_password', $presenceOfNewPassword);
    }
}
