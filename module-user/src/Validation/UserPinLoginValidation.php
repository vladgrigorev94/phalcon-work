<?php

declare(strict_types=1);

namespace PhUser\Validation;

use Phalcon\Filter;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class UserPinLoginValidation extends Validation
{
    public function initialize()
    {
        $presenceOfSessionToken = new PresenceOf(
            [
                'message' => 'Have no session token. Please login again or go to our callcenter',
            ]
        );
        $this->setFilters('auth_session_token', Filter::FILTER_STRIPTAGS);
        $this->add('auth_session_token', $presenceOfSessionToken);

        $presenceOfPin = new PresenceOf(
            [
                'message' => 'pin is required',
            ]
        );
        $this->setFilters('pin', Filter::FILTER_STRIPTAGS);
        $this->add('pin', $presenceOfPin);

        $presenceOfActionName = new PresenceOf(
            [
                'message' => 'Field action_name is required',
            ]
        );
        $this->setFilters('action_name', Filter::FILTER_STRIPTAGS);
        $this->add('action_name', $presenceOfActionName);
    }
}
