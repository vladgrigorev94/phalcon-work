<?php

declare(strict_types=1);

namespace PhUser\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use PhUser\Components\AuthMethods\PgpAuthComponent;
use PhUser\User\UserProvider;

/**
 * Class PgpAuthProvider
 * @package PhUser\Providers
 */
class PgpAuthProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared('pgpAuth',
            function () use ($container) {
                $auth = $container->getShared('auth');

                return new PgpAuthComponent($auth, new UserProvider());
            });
    }
}
