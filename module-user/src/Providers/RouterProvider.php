<?php

declare(strict_types=1);

namespace PhUser\Providers;

use PhCore\Middleware\AuthenticationMiddleware;
use PhCore\Middleware\NotFoundMiddleware;
use PhCore\Middleware\ResponseMiddleware;
use PhCore\Providers\RouterProviderAbstract;

/**
 * Class RouterProvider
 * @package PhUser\Providers
 */
class RouterProvider extends RouterProviderAbstract
{

    /**
     * Returns the array for the middleware with the action to attach
     *
     * @return array
     */
    public function getMiddleware(): array
    {
        return [
            NotFoundMiddleware::class       => 'before',
            AuthenticationMiddleware::class => 'before',
            ResponseMiddleware::class       => 'after',
        ];
    }
}
