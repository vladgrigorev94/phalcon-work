<?php

declare(strict_types=1);

namespace PhUser\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use PhUser\Components\PermissionComponent;
use ServiceClientPermission\Core\PermissionApi;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class PermissionModuleProvider
 * @package PhUser\Providers
 */
class PermissionModuleProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared('permission',
            /**
             * @return PermissionComponent
             */
            function () use ($container) {
                $auth = $container->getShared('auth');
                $token = $auth->getTokenFromRequest();
                $arr = [];
                if (!empty($token)) {
                    $arr['id'] = $auth->user()->id;
                    $arr['username'] = $auth->user()->username;
                }
                $api = new PermissionApi($arr);
                $service = new PermissionService($api);
                if (!empty($token)) {
                    $service->setAuthToken($token);
                }

                /** @var $permissionComponent PermissionComponent */
                $permissionComponent = new PermissionComponent($service);
                return $permissionComponent;
            });
    }
}
