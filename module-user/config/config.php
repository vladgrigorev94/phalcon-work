<?php

return new \Phalcon\Config([
    'app' => [
        'name' => 'module-user',
        'debug' => getenv('APP_DEBUG', false),
        'publicUrl' => getenv('APP_URL'),
        'cryptSalt' => 'eEAfR|_&G&f,+vU]:jFr!!A&+71w1Ms9~8_4L!<@[N@DyaIP_2My|:+.u>/6m,$D',
        'display_errors' => getenv('APP_DEBUG', false) ? 'On' : 'Off',
        'time' => microtime(true)
    ],

    'auth' => [

    ],

    // Monolog settings
    'logger' => [
        'name' => 'phalcon_user_service',
        'path' => getenv('DOCKER') ? 'php://stdout' : BASE_DIR . '/logs/app.log',
        'level' => \Monolog\Logger::DEBUG,
    ],

    'models' => [
        'metadata' => [
            'adapter' => getenv('APP_DEBUG') ? 'Memory' : 'Redis'
        ]
    ]
]);