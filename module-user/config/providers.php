<?php

/**
 * Enabled providers. Order does matter
 */

use PhCore\Providers\ValidationProvider;
use PhUser\Providers\AuthProvider;
use PhUser\Providers\CacheProvider;
use PhCore\Providers\DatabaseProvider;
use PhCore\Providers\ErrorHandlerProvider;
use PhCore\Providers\LoggerProvider;
use PhCore\Providers\ModelsMetadataProvider;
use PhCore\Providers\RequestProvider;
use PhCore\Providers\ResponseProvider;
use PhUser\Providers\ConfigProvider;
use PhUser\Providers\MainAuthProvider;
use PhUser\Providers\MultiFactorAuthProvider;
use PhUser\Providers\PermissionModuleProvider;
use PhUser\Providers\PgpAuthProvider;
use PhUser\Providers\PinAuthProvider;
use PhUser\Providers\RouterProvider;

return [
    ConfigProvider::class,
    LoggerProvider::class,
    AuthProvider::class,
    RequestProvider::class,
    ResponseProvider::class,
    ErrorHandlerProvider::class,
    DatabaseProvider::class,
    ModelsMetadataProvider::class,
    CacheProvider::class,
    RouterProvider::class,
    PermissionModuleProvider::class,
    MultiFactorAuthProvider::class,
    PgpAuthProvider::class,
    PinAuthProvider::class,
    MainAuthProvider::class,
    ValidationProvider::class
];
