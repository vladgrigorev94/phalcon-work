# Docker Phalcon development environment

## Dependencies

- [Docker Engine ~> 18.09.3](https://docs.docker.com/installation/)
- [Docker compose ~> 1.23.2](https://docs.docker.com/compose/install/)

## Usage

    docker-compose up -d --build
    
## Install packages
    
    enter in container -
    docker exec -it phalcon_user_service sh
    in command line - 
    php composer.phar install
## Run migrations
    in command line - 
    php vendor/bin/phinx migrate
## Run seeds
    in command line - 
    php vendor/bin/phinx seed:run
    exit from container

    
## Generate Docs

`doxygen Doxyfile` (generated docs folder)