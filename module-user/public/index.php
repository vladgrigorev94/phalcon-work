<?php

error_reporting(E_ALL);
define('BASE_DIR', dirname(__DIR__));
define('APP_NAME', 'module-user');
require_once BASE_DIR . '/vendor/autoload.php';

$bootstrap = new PhCore\Bootstrap\Api();
$bootstrap->setup();
$bootstrap->run();