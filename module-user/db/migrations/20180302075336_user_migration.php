<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class UserMigration extends AbstractMigration
{
    public function change()
    {
        // create the table
        $table = $this->table('users', ['signed' => false]);
        $table
            ->addColumn('username',  Literal::from('varchar'), ['null' => false])
            ->addColumn('password',  Literal::from('varchar'), [
                'null' => false
            ])
            ->create();
    }
}
