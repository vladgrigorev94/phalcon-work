Сайт с формой входа,  регистрации и ресет пароля. 

## Run permission tests
    vendor/api-client/service-permission/vendor/bin/phpunit vendor/api-client/service-permission/tests --bootstrap vendor/autoload.php 
## Run user tests
    vendor/api-client/service-user/vendor/bin/phpunit vendor/api-client/service-user/tests --bootstrap vendor/autoload.php     
## Run tests
    vendor/bin/phpunit tests --bootstrap vendor/autoload.php    
## Generate apidoc
    first create a apidoc folder
    ./vendor/bin/openapi -o /var/www/apidoc/apidoc.yaml /var/www/src/swagger
    next get apidoc.yaml and past to https://editor.swagger.io
    