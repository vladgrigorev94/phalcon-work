<?php

error_reporting(E_ALL);
(new \Phalcon\Debug)->listen();
/**
 * Define some useful constants
 */
define('BASE_DIR', dirname(__DIR__));
define('APP_PATH', BASE_DIR . '/src');
require_once BASE_DIR . '/vendor/autoload.php';
