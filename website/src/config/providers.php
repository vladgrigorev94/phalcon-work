<?php

return [
    // Application Service Providers
    MWS\Provider\Config\ServiceProvider::class,
    MWS\Provider\Url::class,
    MWS\Provider\Logger::class,
    MWS\Provider\Crypt::class,
    MWS\Provider\Session::class,
    MWS\Provider\View::class,
    MWS\Provider\Router::class,
    MWS\Provider\Dispatcher::class,
    MWS\Provider\FileSystem::class,
    MWS\Provider\FlashService::class,
    MWS\Provider\Auth::class,
    //in future I want to fix it
    MWS\Provider\UserService::class,
    MWS\Provider\UserComponent::class,
    MWS\Provider\PermissionService::class,
    MWS\Provider\PermissionComponent::class
    // Third Party Providers
];
