<?php

return [
    'db'   => null,
    'application' => [
        'name'           => 'website',
        'debug'          => getenv('APP_DEBUG') === 'true' ? true : false,
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'formsDir'       => APP_PATH . '/forms/',
        'viewsDir'       => APP_PATH . '/views/',
        'libraryDir'     => APP_PATH . '/libs/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'cacheDir'       => BASE_DIR . '/storage/',
        'baseUri'        => '/',
        'publicUrl'      => 'site.phalcon.local',
        'cryptSalt'      => 'r!!A&+71wAfR|_&G&f,+v1Ms9~8_4eEU]:jFL!<@[N@|:+.u>/6m,$DDyaIP_2My'
    ],

    // Monolog settings
    'logger' => [
        'name' => 'phalcon_website',
        'path' =>  BASE_DIR . '/logs/app.log',
        'docker' => 'php://stderr',
        'level' => \Monolog\Logger::DEBUG,
    ],

    'redis' => [
        'host' => getenv('REDIS_HOST'),
        'port' => getenv('REDIS_PORT')
    ],
    // Set to false to disable sending emails (for use in test environment)
    'models' => [
        'metadata' => [
            'adapter' => getenv('APP_DEBUG') ? 'Memory' : 'Redis'
        ]
    ]
];
