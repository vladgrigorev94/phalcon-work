<?php
//I couldn't find any cache service
return [
    'default' => env('CACHE_DRIVER', 'file'),

    'views'   => env('VIEW_CACHE_DRIVER', 'volt'),

    'drivers' => [

        'apc' => [
            'adapter' => 'Apc',
        ],

        'file' => [
            'adapter'  => 'File',
            'cacheDir' => cache_path('data') . '/'
        ],

        'volt' => [
            'adapter'  => 'File',
            'cacheDir' => storage_path('volt') . '/'
        ],

        'redis' => [
            'adapter' => 'Redis',
            'host'    => env('REDIS_HOST', '127.0.0.1'),
            'port'    => env('REDIS_PORT', 6379),
            'index'   => env('REDIS_INDEX', 0),
        ],

        'memory' => [
            'adapter' => 'Memory',
        ],
    ],

    'prefix' => env('CACHE_PREFIX', 'mws_cache_'),

    'lifetime' => env('CACHE_LIFETIME', 86400),
];
