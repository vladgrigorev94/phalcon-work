<?php

namespace MWS\Models;

use Phalcon\Filter;
use Phalcon\Mvc\Model;

/**
 * Class User
 * @package MWS\Models
 */
class User extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="username", type="string", length=255, nullable=false)
     */
    public $username;

    /**
     *
     * @var string
     * @Column(column="password", type="string", length=255, nullable=false)
     */
    public $password;

    /**
     *
     * @var integer
     * @Column(column="auth_method_id", type="integer", length=32, nullable=false)
     */
    public $auth_method_id;

    /**
     *
     * @var integer
     * @Column(column="role_id", type="integer", length=32, nullable=false)
     */
    public $role_id;

    /**
     * @return null
     */
    public function initialize()
    {
        $this->belongsTo(
            'auth_method_id',
            'MWS\Models\AuthMethod',
            'id'
        );
        $this->belongsTo(
            'role_id',
            'MWS\Models\Role',
            'id'
        );
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'id',
            'username'
        ];
    }

    /**
     * Model filters
     *
     * @return array<string,string,string>
     */
    public function getModelFilters(): array
    {
        return [
            'id'            => Filter::FILTER_ABSINT,
            'username'      => Filter::FILTER_STRING,
            'password'      => Filter::FILTER_STRING
        ];
    }
}
