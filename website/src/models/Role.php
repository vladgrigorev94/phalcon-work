<?php

namespace MWS\Models;

use Phalcon\Mvc\Model;

/**
 * Class Role
 * @package MWS\Models
 */
class Role extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=255, nullable=false)
     */
    public $name;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'User', 'role_id');
    }
    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'id',
            'name'
        ];
    }
}
