<?php

namespace MWS\Provider;

/**
 * MWS\Provider\Routing\ServiceProvider
 *
 * @package MWS\Provider\Routing
 */
class Router extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'router';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                return require config_path() . '/routes.php';
            }
        );
    }
}
