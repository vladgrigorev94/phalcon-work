<?php

namespace MWS\Provider;

use ServiceClientPermission\Core\PermissionApi;

/**
 * MWS\Provider\Security\ServiceProvider
 *
 * @package MWS\Provider\Security
 */
class PermissionService extends AbstractServiceProvider
{

    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'permissionService';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $context = container('session')->get('auth') ?? [];
                $api = new PermissionApi($context);
                $service = new \ServiceClientPermission\Services\PermissionService($api);
                $service->setAuthToken($context['token'] ??'');

                return $service;
            }
        );
    }
}
