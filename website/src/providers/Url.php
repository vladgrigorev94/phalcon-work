<?php

namespace MWS\Provider;

use Phalcon\Mvc\Url as UrlResolver;

/**
 * MWS\Provider\UrlResolver\ServiceProvider
 *
 * @package MWS\Provider\UrlResolver
 */
class Url extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'url';

    /**
     * {@inheritdoc}
     * The URL component is used to generate all kind of urls in the application.
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $config = container('config');

                $url = new UrlResolver();
                $url->setBaseUri($config->application->baseUri);
                return $url;
            }
        );
    }
}
