<?php

namespace MWS\Provider;

/**
 * MWS\Provider\Security\ServiceProvider
 *
 * @package MWS\Provider\Security
 */
class PermissionComponent extends AbstractServiceProvider
{

    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'permissionComponent';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $service = container('permissionService');

                return new \MWS\Components\PermissionComponent($service);
            }
        );
    }
}
