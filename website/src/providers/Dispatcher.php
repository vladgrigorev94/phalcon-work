<?php

namespace MWS\Provider;

use Phalcon\Mvc\Dispatcher as DisResolver;
use MWS\Listener\DispatcherListener;

/**
 * MWS\Provider\Dispatcher\ServiceProvider
 *
 * @package MWS\Provider\Dispatcher
 */
class Dispatcher extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'dispatcher';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {

                $dispatcher = new DisResolver();
                $dispatcher->setDefaultNamespace('MWS\Controllers');

                //custom dispatch listener for customize exceptions
                container('eventsManager')->attach('dispatch', new DispatcherListener(container()));
                //here we set our middlewares
                container('eventsManager')->attach(
                    'dispatch:beforeExecuteRoute',
                    new \Sid\Phalcon\AuthMiddleware\Event()
                );

                container('eventsManager')->attach(
                    'dispatch:afterExecuteRoute',
                    new \MWS\AfterRouteMiddleware\Event()
                );

                $dispatcher->setDI(container());
                $dispatcher->setEventsManager(container('eventsManager'));

                return $dispatcher;
            }
        );
    }
}
