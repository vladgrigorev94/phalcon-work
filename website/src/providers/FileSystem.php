<?php

namespace MWS\Provider;

use League\Flysystem\Filesystem as LeagueFS;
use League\Flysystem\Adapter\Local;

/**
 * MWS\Provider\FileSystem\ServiceProvider
 *
 * @package MWS\Provider\FileSystem
 */
class FileSystem extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'filesystem';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->set(
            $this->serviceName,
            function ($root = null) {
                if ($root === null) {
                    $root = dirname(app_path());
                }
                //set an class for work with different filesystems
                return new LeagueFS(new Local($root));
            }
        );
    }
}
