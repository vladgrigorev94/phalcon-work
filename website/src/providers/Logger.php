<?php

namespace MWS\Provider;

/**
 * MWS\Provider\Logger\ServiceProvider
 *
 * @package MWS\Provider\Logger
 */
class Logger extends AbstractServiceProvider
{

    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'logger';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {

        $this->di->set(
            $this->serviceName,
            function () {
                $config = container('config');
                $logger = new \Monolog\Logger($config->logger->name);
                $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($config->logger->path, $config->logger->level));
                $logger->pushHandler(new \Monolog\Handler\StreamHandler($config->logger->docker, $config->logger->level));
                return $logger;
            }
        );
    }
}
