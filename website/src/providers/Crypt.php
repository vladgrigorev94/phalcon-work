<?php

namespace MWS\Provider;

/**
 * MWS\Provider\Security\ServiceProvider
 *
 * @package MWS\Provider\Security
 */
class Crypt extends AbstractServiceProvider
{

    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'crypt';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $config = container('config');
                $crypt = new \Phalcon\Crypt();
                $crypt->setKey($config->application->cryptSalt);
                return $crypt;
            }
        );
    }
}
