<?php

namespace MWS\Provider;


use Phalcon\Flash\Direct;
use Phalcon\Flash\Session;

/**
 * MWS\Provider\Flash\ServiceProvider
 *
 * @package MWS\Provider\Flash
 */
class FlashService extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'flash';

    protected $cssStyles = [
        'error'   => 'alert alert-danger fade in',
        'success' => 'alert alert-success fade in',
        'notice'  => 'alert alert-info fade in',
        'warning' => 'alert alert-warning fade in',
    ];

    /**
     * {@inheritdoc}
     *
     * Register the Flash Service with the Twitter Bootstrap3 classes.
     *
     * @return void
     */
    public function register()
    {
        $cssStyles = $this->cssStyles;
        $this->di->set(
            $this->serviceName,
            function () use ($cssStyles) {
                $flash = new Direct($cssStyles);

                $flash->setAutoescape(true);
                $flash->setDI(container());
                $flash->setCssClasses($cssStyles);

                return $flash;
            }
        );

        $this->di->setShared(
            'flashSession',
            function () use ($cssStyles) {
                $flash = new Session($cssStyles);

                $flash->setAutoescape(true);
                $flash->setDI(container());
                $flash->setCssClasses($cssStyles);

                return $flash;
            }
        );
    }
}
