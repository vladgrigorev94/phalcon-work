<?php

namespace MWS\Provider;

/**
 * MWS\Provider\Security\ServiceProvider
 *
 * @package MWS\Provider\Security
 */
class Auth extends AbstractServiceProvider
{

    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'auth';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                return new \MWS\Auth\Auth();
            }
        );
    }
}
