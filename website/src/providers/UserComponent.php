<?php

namespace MWS\Provider;

/**
 * MWS\Provider\Security\ServiceProvider
 *
 * @package MWS\Provider\Security
 */
class UserComponent extends AbstractServiceProvider
{

    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'userComponent';

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register()
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $service = container('userService');

                return new \MWS\Components\UserComponent($service);
            }
        );
    }
}
