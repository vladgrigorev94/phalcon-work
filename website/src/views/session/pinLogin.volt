{{ content() }}

<div align="center">

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>Pin log in</h2>
    </div>
    <table class="signup updateUser">
        <tr>
            <td align="right">{{ form.label('code') }}</td>
            <td>
                {{ form.render('code') }}
                {{ form.messages('code') }}
            </td>
        </tr>
    </table>

    {{ form.render('csrf', ['value': security.getToken()]) }}
    {{ form.messages('csrf') }}
    <div>
        {{ form.render('Sign In') }}
    </div>
    {{ end_form() }}

</div>