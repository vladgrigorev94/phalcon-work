{{ content() }}

<div class="well">

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>PGP log In</h2>
    </div>
        <textarea style="width: 100%;height: 350px;" readonly onclick="this.select()">{{ encryptedHash }}</textarea>
    <div style="margin: 10px 0px;">
        {{ form.label('decodedString') }}
        {{ form.render('decodedString') }}
        {{ form.messages('decodedString') }}
    </div>

    {{ form.render('csrf', ['value': security.getToken()]) }}

    <div>
        {{ form.render('Sign In') }}
    </div>

    </form>

</div>