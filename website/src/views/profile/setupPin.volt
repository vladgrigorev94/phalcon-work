{{ content() }}

<div>

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>Setup PIN code</h2>
    </div>

    {% if pinEnabled %}
        <div class="text-success">PIN Code Enabled</div>
        <a href="/profile/disablePin" class="btn btn-primary">Disable PIN</a>
    {% else %}
        <table class="signup updateUser">
            <tr>
                <td align="right">{{ form.label('code') }}</td>
                <td>
                    {{ form.render('code') }}
                    {{ form.messages('code') }}
                </td>
            </tr>
        </table>

        {{ form.render('csrf', ['value': security.getToken()]) }}
        {{ form.messages('csrf') }}
        <div>
            {{ form.render('Send') }}
        </div>
        {{ end_form() }}
    {% endif %}



</div>