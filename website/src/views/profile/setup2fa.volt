{{ content() }}

<div>
    <div align="left">
        <h2>Setup 2fa</h2>
    </div>
    {% if twoFaEnabled %}
        <div class="text-success">2FA Enabled</div>
        <a href="/profile/disableTwoFactory" class="btn btn-primary">Disable 2FA</a>
    {% else %}
        {{ form('class': 'form-search') }}


        <div align="center">
            Please scan QR-code below in your app and type timing password
        </div>
        <img src="{{ qrCode }}" width="200"/>
        <table class="signup updateUser">
            <tr>
                <td align="right">{{ form.label('code') }}</td>
                <td>
                    {{ form.render('code') }}
                    {{ form.messages('code') }}
                </td>
            </tr>
        </table>

        {{ form.render('csrf', ['value': security.getToken()]) }}
        {{ form.messages('csrf') }}
        <div>
            {{ form.render('Send') }}
        </div>
        {{ end_form() }}
    {% endif %}

</div>