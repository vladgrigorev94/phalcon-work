{{ content() }}

<div>

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>Setup PGP Key</h2>
    </div>

    {% if pgpEnabled %}
        <div class="text-success">PGP Key Enabled</div>
        <a href="/profile/disablePgp" class="btn btn-primary">Disable PGP</a>
    {% else %}
        <table class="signup setupPgp">
            <tr>
                <td align="right">{{ form.label('publicKey') }}</td>
                <td>
                    {{ form.render('publicKey') }}
                    {{ form.messages('publicKey') }}
                </td>
            </tr>

            <tr>
                <td align="right"></td>
                <td>{{ form.render('Send') }}</td>
            </tr>
        </table>
    {% endif %}


    {{ form.render('csrf', ['value': security.getToken()]) }}
    {{ form.messages('csrf') }}
    {{ end_form() }}

</div>