{{ content() }}

<div class="well">

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>Verify 2fa</h2>
    </div>

    {{ form.label('code') }}
    {{ form.render('code') }}
    {{ form.messages('code') }}


    {{ form.render('csrf', ['value': security.getToken()]) }}

    <div>
        {{ form.render('Sign In') }}
    </div>

    </form>

</div>