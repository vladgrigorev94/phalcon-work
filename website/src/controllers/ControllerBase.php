<?php

namespace MWS\Controllers;

use MWS\Auth\Auth;
use MWS\Components\PermissionComponent;
use Phalcon\Mvc\Controller;
use Phalcon\Dispatcher;
use ServiceClientPermission\Services\PermissionService;

/**
 * ControllerBase
 * This is the base controller for all controllers where authentication must be checked
 * @property Auth $auth
 * @property PermissionService $permissionService
 * @property PermissionComponent $permissionComponent
 */
class ControllerBase extends Controller
{
    public function userId()
    {
        return $this->session->get('auth')['id'] ?? null;
    }

    // this will be base controller for all endpoints where needs authentication
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $res = $this->userService->send('auth.doCheck', []);
        if ($res->hasError()) {
            $this->session->destroy();
            $this->session->start();
            $this->response->redirect('/session/login');

            return false;
        }

        return true;
    }
}
