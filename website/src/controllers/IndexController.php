<?php
namespace MWS\Controllers;

use Phalcon\Mvc\Controller;

/**
 * Display the default index page.
 */
class IndexController extends Controller
{

    /**
     * @AuthMiddleware("MWS\Middleware\RedirectOnAuth")
     * user login
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('public');
    }
}
