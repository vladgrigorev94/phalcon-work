<?php

namespace MWS\Controllers;

use MWS\Forms\ChangePasswordForm;
use MWS\Forms\LoginForm;
use MWS\Forms\PgpLoginForm;
use MWS\Forms\PinLoginForm;
use MWS\Forms\Setup2faForm;
use MWS\Forms\SignUpForm;
use MWS\Forms\ForgotPasswordForm;
use Phalcon\Dispatcher;
use Phalcon\Mvc\Controller;

/**
 * Controller used handle non-authenticated session actions like login/logout, user signup, and forgotten passwords
 */
class SessionController extends Controller
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('public');
    }

    /**
     * @param Dispatcher $dispatcher
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth-session') ?? [];
        $userId = $auth['user_id'] ?? null;
        if (!empty($userId)) {
            $auth = $this->session->get('auth-session');
            $res = $this->userComponent->verifyToken($userId, $auth['auth_session_token']);
            if (!$res->hasError()) {
                $this->session->set('auth', [
                    'id' => $res->data['id'],
                    'username' => $res->data['username'],
                    'token' => $res->data['token']
                ]);
            }
        }
    }

    /**
     * Allow a user to signup to the system
     */
    public function signupAction()
    {
        $form = new SignUpForm();

        if ($form->check()) {
            return $this->response->redirect('/');
        }

        $this->view->form = $form;
    }

    /**
     * @AuthMiddleware("MWS\Middleware\RedirectOnAuth")
     * user login
     */
    public function loginAction()
    {
        $form = new LoginForm();

        if ($form->check()) {
            return $this->response->redirect('/');
        }

        $this->view->form = $form;
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function fa2LoginAction()
    {
        $form = new Setup2faForm();

        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $auth = $this->session->get('auth-session');
            $response = $this->auth->login2fa([
                'authSessionToken' => $auth['auth_session_token'],
                'code' => $this->request->getPost('code')
            ]);
            if (!$response->hasError()) {
                $this->flash->success('Success!');
                if (isset($response->data['auth_session_token']) && isset($response->data['action'])) {
                    $this->session->set('auth-session', [
                        'auth_session_token' => $response->data['auth_session_token'],
                        'action' => $response->data['action'],
                        'user_id' => $auth['user_id']
                    ]);
                }

                return $this->response->redirect('/');
            } else {
                $error = $response->getError();
                $this->flash->error($error['message']);
                if (isset($error->fields)) {
                    $form->setMessages($error->fields);
                }

            }
        }

        $this->view->form = $form;
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function pgpLoginAction()
    {
        $form = new PgpLoginForm();
        $auth = $this->session->get('auth-session') ?? [];

        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $response = $this->auth->loginPgp([
                'authSessionToken' => $auth['auth_session_token'],
                'decodedString' => $this->request->getPost('decodedString')
            ]);

            if (!$response->hasError()) {
                $this->flash->success('Success!');
                if (isset($response->data['auth_session_token']) && isset($response->data['action'])) {
                    $this->session->set('auth-session', [
                        'auth_session_token' => $response->data['auth_session_token'],
                        'action' => $response->data['action'],
                        'user_id' => $auth['user_id']
                    ]);
                }

                return $this->response->redirect('/');
            } else {
                $error = $response->getError();
                $this->flash->error($error['message']);
                if (isset($error->fields)) {
                    $form->setMessages($error->fields);
                }
            }
        }
        $res = $this->permissionComponent->getEncryptHash($auth['user_id']);
        $this->view->encryptedHash = $res->data['encrypt_hash'];
        $this->view->form = $form;
    }

    public function pinLoginAction()
    {
        $form = new PinLoginForm();
        $auth = $this->session->get('auth-session') ?? [];
        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $response = $this->auth->loginPin([
                'authSessionToken' => $auth['auth_session_token'],
                'pin' => $this->request->getPost('code')
            ]);
            if (!$response->hasError()) {
                $this->flash->success('Welcome back user');
                if (isset($response->data['auth_session_token']) && isset($response->data['action'])) {
                    $this->session->set('auth-session', [
                        'auth_session_token' => $response->data['auth_session_token'],
                        'action' => $response->data['action']
                    ]);
                }

                return $this->response->redirect('/');
            } else {
                $error = $response->getError();
                $this->flash->error($error->message);
                if (isset($error->fields)) {
                    $form->setMessages($error->fields);
                }

            }
        }

        $this->view->form = $form;
    }

    public function setPasswordAction()
    {
        $form = new ChangePasswordForm();
        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            $userId = $this->auth->getIdentity()['id'];
            $oldPassword = $this->request->getPost('password');
            $newPassword = $this->request->getPost('newPassword');
            $response = $this->userComponent->setPassword($userId, $oldPassword, $newPassword);
            if (!$response->hasError()) {
                $this->flash->success('Success!');

                return $this->response->redirect('/');
            } else {
                $error = $response->getError();
                $this->flash->error('Error!');
                if (isset($error->fields)) {
                    $form->setMessages($error->fields);
                }
            }
        }
        $this->view->form = $form;
    }

    /**
     * Shows the forgot password form
     */
    public function forgotPasswordAction()
    {
        $form = new ForgotPasswordForm();
        if ($this->request->isPost()) {
            // todo Send emails only is config value is set to true
            if ($form->isValid($this->request->getPost()) === false) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                $this->flash->warning('Emails are currently disabled. Change config key "useMail" to true to enable emails.');

                return $this->response->redirect('/');
            }
        }
        $this->view->form = $form;
    }

    /**
     * Closes the session
     */
    public function logoutAction()
    {
        $this->auth->logout();
        $this->flash->warning('you are logged out.');

        return $this->response->redirect('/');
    }
}
