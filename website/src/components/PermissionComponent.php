<?php

namespace MWS\Components;

use Phalcon\Mvc\User\Component;
use ServiceClient\Core\ResponseInterface;
use ServiceClientPermission\Services\PermissionService;

/**
 * Class PermissionComponent
 * @package MWS\Components
 */
class PermissionComponent extends Component
{
    /**
     * @var PermissionService
     */
    private $service;

    /**
     * PermissionComponent constructor.
     * @param PermissionService $service
     */
    public function __construct(PermissionService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $userId
     * @param $authMethodId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function setAuthMethod($userId, $authMethod): ResponseInterface
    {
        return $this->service->send('authMethod.setAuthMethod', [
            'user_id' => $userId,
            'auth_method' => $authMethod
        ]);
    }

    /**
     * @param int $userId
     * @param string $method
     * @return ResponseInterface|null
     * @throws \Exception
     */
    public function disableAuthMethod(int $userId, string $method): ?ResponseInterface
    {
        $res = $this->service->send('authMethod.show', [
            'user_id' => $userId
        ]);
        $authMethodId = current(array_column(array_filter($res->data['auth_methods'] ?? [], function($item) use ($method) {
            return $item['name'] === $method;
        }), 'id'));
        if ($authMethodId === false) {
            return null;
        }

        return $this->service->send('authMethod.disableAuthMethod', [
            'user_id' => $userId,
            'auth_method_id' => $authMethodId
        ]);
    }

    /**
     * @param int $userId
     * @param string $method
     * @return bool
     * @throws \Exception
     */
    public function hasAuthMethod(int $userId, string $method): bool
    {
        $res = $this->service->send('authMethod.show', [
            'user_id' => $userId
        ]);
        return in_array($method, array_column($res->data['auth_methods'] ?? [], 'name'));
    }

    /**
     * @param $userId
     * @param $roleId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function setRole($userId, $roleId): ResponseInterface
    {
        $response = $this->service->send('role.update', [
            'user_id' => $userId,
            'role_id' => $roleId
        ]);

        return $response;
    }

    /**
     * @return array|ResponseInterface
     * @throws \Exception
     */
    public function listAuthMethods(): array
    {
        $authMethods = $this->service->send('authMethod.index', []);
        if($authMethods->hasError()) {
            return $authMethods->getError();
        }
        $authMethods = $authMethods->toArray();
        $authMethods = $authMethods['data']['auth_methods'];
        $array = [];
        array_map(function ($value) use (&$array) {
            $array[$value['id']] = $value['name'];
        }, $authMethods);

        return $array;
    }

    /**
     * @return array|ResponseInterface
     * @throws \Exception
     */
    public function listRoles(): array
    {
        $roles = $this->service->send('role.index', []);
        if($roles->hasError()) {
            return $roles;
        }
        $roles = $roles->toArray();
        $roles = $roles['data']['roles'];
        $array = [];
        array_map(function ($value) use (&$array) {
            $array[$value['id']] = $value['name'];
        }, $roles);

        return $array;
    }

    /**
     * @param int $userId
     * @param array $userData
     * @return ResponseInterface
     * @throws \Exception
     */
    public function makePGP(int $userId, array $userData): ResponseInterface
    {
        return $this->service->send('pgp.makePGP', [
            'user_id' => $userId,
            'public_key' => $userData['publicKey']
        ]);
    }

    /**
     * @param int $userId
     * @param array $userData
     * @return ResponseInterface
     * @throws \Exception
     */
    public function makePin(int $userId, array $userData): ResponseInterface
    {
        return $this->service->send('pin.makePin', [
            'user_id' => $userId,
            'pin' => $userData['pin']
        ]);
    }

    /**
     * @param int $userId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getEncryptHash(int $userId): ResponseInterface
    {
        return $this->service->send('pgp.getEncryptHash', [
            'user_id' => $userId
        ]);
    }

    /**
     * @param string $authSessionToken
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getUserIdByAuthSessionToken(string $authSessionToken): ResponseInterface
    {
        return $this->service->send('action.getUserIdByAuthSessionToken', [
            'auth_session_token' => $authSessionToken
        ]);
    }
}
