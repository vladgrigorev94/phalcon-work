<?php

namespace MWS\Components;

use Phalcon\Mvc\User\Component;
use ServiceClient\Core\ResponseInterface;
use ServiceClientUser\Services\UserService;

/**
 * Class UserComponent
 * @package MWS\Components
 */
class UserComponent extends Component
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * UserComponent constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $userId
     * @param $oldPassword
     * @param $newPassword
     * @return ResponseInterface
     * @throws \Exception
     */
    public function setPassword($userId, $oldPassword, $newPassword): ResponseInterface
    {
        return $this->service->send('auth.changePassword', [
            'user_id' => $userId,
            'password' => $oldPassword,
            'new_password' => $newPassword
        ]);
    }

    /**
     * @param $userId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getProfile($userId): ResponseInterface
    {
        return $this->service->send('user.getProfile', [
            'user_id' => $userId
        ]);
    }

    /**
     * @param int $userId
     * @param array $userData
     * @return ResponseInterface
     * @throws \Exception
     */
    public function updateProfile(int $userId, array $userData): ResponseInterface
    {
        return $this->service->send('user.update', [
            'user_id' => $userId,
            'username' => $userData['username']
        ]);
    }

    /**
     * @param int $userId
     * @param string $code
     * @return ResponseInterface
     * @throws \Exception
     */
    public function verify2faCode(int $userId, string $code): ResponseInterface
    {
        return $this->service->send('multiFactorAuth.verify2fa', [
            'user_id' => $userId,
            'code' => $code
        ]);
    }

    /**
     * @param int $userId
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getQrCode(int $userId): ResponseInterface
    {
        return $this->service->send('multiFactorAuth.getQrCode', [
            'user_id' => $userId
        ]);
    }

    /**
     * @param int $userId
     * @param string $authSessionToken
     * @return ResponseInterface
     * @throws \Exception
     */
    public function verifyToken(int $userId, string $authSessionToken): ResponseInterface
    {
        return $this->service->send('auth.verifyToken', [
            'user_id' => $userId,
            'auth_session_token' => $authSessionToken
        ]);
    }
}
