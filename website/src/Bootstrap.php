<?php

namespace MWS;

use Phalcon\Di;
use Phalcon\DiInterface;
use MWS\Provider;
use InvalidArgumentException;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application as MvcApplication;
use MWS\Provider\ServiceProviderInterface;

/**
 * Class Bootstrap
 * @package MWS
 */
class Bootstrap
{
    /**
     * The internal application core.
     * @var \Phalcon\Application
     */
    private $app;

    /**
     * The Dependency Injection Container
     * @var DiInterface
     */
    private $di;

    /**
     * Current application environment:
     * production, staging, development, testing
     * @var string
     */
    private $environment;

    /**
     * Bootstrap constructor.
     */
    public function __construct()
    {

        $this->di = new FactoryDefault();

        $this->di->setShared('bootstrap', $this);

        Di::setDefault($this->di);

        /**
         * These services should be registered first
         */
        $this->initializeServiceProvider(new Provider\EventsManager($this->di));

        $this->setupEnvironment();

        $this->createInternalApplication();

        /** @noinspection PhpIncludeInspection */
        $providers = require config_path('providers.php');
        if (is_array($providers)) {
            $this->initializeServiceProviders($providers);
        }

        $this->app->setEventsManager(container('eventsManager'));

        $this->di->setShared('app', $this->app);
        $this->app->setDI($this->di);
    }

    /**
     * Runs the Application
     *
     * @return mixed
     */
    public function run()
    {
        return $this->getOutput();
    }

    /**
     * Get the Application.
     *
     * @return \Phalcon\Application
     */
    public function getApplication()
    {
        return $this->app;
    }

    /**
     * Get application output.
     *
     * @return string
     */
    public function getOutput()
    {
        if ($this->app instanceof MvcApplication) {
            return $this->app->handle()->getContent();
        }

        return $this->app->handle();
    }

    /**
     * Gets current application environment: production, staging, development, testing, etc.
     *
     * @return string
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * Initialize the Service Providers.
     *
     * @param  string[] $providers
     * @return $this
     */
    protected function initializeServiceProviders(array $providers)
    {
        foreach ($providers as $name => $class) {
            $this->initializeServiceProvider(new $class($this->di));
        }

        return $this;
    }

    /**
     * Initialize the Service Provider.
     *
     * Usually the Service Provider register a service in the Dependency Injector Container.
     *
     * @param  ServiceProviderInterface $serviceProvider
     * @return $this
     */
    protected function initializeServiceProvider(ServiceProviderInterface $serviceProvider)
    {
        $serviceProvider->register();
        $serviceProvider->boot();

        return $this;
    }

    /**
     * Create internal application to handle requests.
     *
     * @throws InvalidArgumentException
     */
    protected function createInternalApplication()
    {
        $this->app = new MvcApplication($this->di);
    }

    /**
     * Setting up the application environment.
     *
     * This tries to get `APP_ENV` environment variable from $_ENV.
     * If failed the `development` will be used.
     *
     * After getting `APP_ENV` variable we set the Bootstrap::$environment
     * and the `APPLICATION_ENV` constant.
     */
    protected function setupEnvironment()
    {
        $this->environment = env('APP_ENV', 'development');

        defined('APPLICATION_ENV') || define('APPLICATION_ENV', $this->environment);

        $this->initializeServiceProvider(new Provider\Environment($this->di));
    }
}

