<?php

namespace MWS\Swagger\Permission\Controllers;

use MWS\Swagger\MainController;

/**
 * Class PinController
 * @package MWS\Swagger\Permission\Controllers
 */
class PinController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Make pin",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="pin.makePin"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function makePin()
    {
        //user_id
        //pin
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Verify pin",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="pin.verifyPin"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function verifyPin()
    {
        //user_id
        //pin
    }
}