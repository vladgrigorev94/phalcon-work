<?php

namespace MWS\Swagger\Permission\Controllers;

use MWS\Swagger\MainController;

/**
 * Class RoleController
 * @package MWS\Swagger\Permission\Controllers
 */
class RoleController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     summary="List all auth methods",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="role.index"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function index()
    {
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Show user_id role",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="role.show"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function show()
    {
        //user_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Update user role",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="role.update"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function update()
    {
        //user_id
        //role_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Delete user role",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="role.delete"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function delete()
    {
        //user_id
    }
}