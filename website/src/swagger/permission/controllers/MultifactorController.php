<?php

namespace MWS\Swagger\Permission\Controllers;

use MWS\Swagger\MainController;

/**
 * Class MultifactorController
 * @package MWS\Swagger\Permission\Controllers
 */
class MultifactorController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Get google auth secret",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="multifactor.getGoogleAuthSecret"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function getGoogleAuthSecret()
    {
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Set google auth secret",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="multifactor.setGoogleAuthSecret"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function setGoogleAuthSecret()
    {
    }

}