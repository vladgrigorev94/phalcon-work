<?php

namespace MWS\Swagger\Permission\Controllers;

use MWS\Swagger\MainController;

/**
 * Class ActionController
 * @package MWS\Swagger\Permission\Controllers
 */
class ActionController extends MainController
{
    /**
     * @OA\Post(
     *      path="/call",
     *      tags={"permission"},
     *      summary="Generate action",
     *      operationId="generate",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="method",
     *                      type="string",
     *                      example="action.generate"
     *                  ),
     *                  @OA\Property(
     *                      property="params",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(
     *                              property="user_id",
     *                              type="integer",
     *                              example="1"
     *                          ),
     *                          @OA\Property(
     *                              property="action_name",
     *                              type="string",
     *                              example="12345"
     *                          ),
     *                          @OA\Property(
     *                              property="auth_methods",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="string",
     *                                  enum = {"2fa", "pgp", "pin"},
     *                              )
     *                          )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                         @OA\Property(
     *                             property="action",
     *                             type="string",
     *                             example="ok"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function generate()
    {
        //user_id
        //action_name
        //auth_methods
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Set user' completed auth in action",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="action.completeAuthMethod"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         ),
     *                         @OA\Property(
     *                             property="action_name",
     *                             type="string",
     *                             example="doLogin"
     *                         ),
     *                         @OA\Property(
     *                             property="auth_method",
     *                             type="string",
     *                             example="2fa"
     *                         ),
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="action",
     *                             type="string",
     *                             example=""
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function completeAuthMethod()
    {
        //user_id
        //action_name
        //auth_method
        //auth_session_token
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Check that action' auth methods was finished and set is_completed flag",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="action.setActionComplete"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         ),
     *                         @OA\Property(
     *                             property="action_name",
     *                             type="string",
     *                             example="doLogin"
     *                         ),
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="complete",
     *                             type="string",
     *                             example=true
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function setActionComplete()
    {
        //user_id
        //action_name
        //auth_session_token
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Check user' completed auth",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="action.getUserIdByAuthSessionToken"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example=1
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function getUserIdByAuthSessionToken()
    {
        //auth_session_token
    }

}