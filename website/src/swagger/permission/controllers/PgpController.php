<?php

namespace MWS\Swagger\Permission\Controllers;

use MWS\Swagger\MainController;

/**
 * Class PgpController
 * @package MWS\Swagger\Permission\Controllers
 */
class PgpController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Generate pgp",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="pgp.makePGP"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function makePGP()
    {
        //user_id
        //public_key
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Get encrypted hash",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="pgp.getEncryptHash"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function getEncryptHash()
    {
        //user_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     summary="Verify decrypt hash",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="pgp.verifyDecryptHash"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function verifyDecryptHash()
    {
        //user_id
        //decrypt_hash
    }
}