<?php

namespace MWS\Swagger\User\Controllers;

use MWS\Swagger\MainController;

/**
 * Class PgpAuthController
 * @package MWS\Swagger\User\Controllers
 */
class PgpAuthController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Do pgp auth",
     *     operationId="doPGP",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="pgpAuth.doPGP"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                         @OA\Property(
     *                             property="decoded_string",
     *                             type="string",
     *                             example="123456"
     *                         ),
     *                         @OA\Property(
     *                             property="action_name",
     *                             type="string",
     *                             example="doLogin"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="success!"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function doPGP()
    {
        //auth_session_token
        //decoded_string
        //action_name
    }
}