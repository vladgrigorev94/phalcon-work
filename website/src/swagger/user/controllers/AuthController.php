<?php

namespace MWS\Swagger\User\Controllers;

use MWS\Swagger\MainController;

/**
 * Class AuthController
 * @package MWS\Swagger\User\Controllers
 */
class AuthController extends MainController
{

    /**
     * @OA\Post(
     *     path="/call/gi",
     *     tags={"user"},
     *     summary="There are three users for test auth response:- admin admin,- user2fa user2fa,- userPGP userPGP,- userPin userPin",
     *     operationId="doLogin",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="auth.doLogin"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="username",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="password",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="id",
     *                             type="integer",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="username",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="token",
     *                             type="string",
     *                             example="Bearer v2.local.sqFFXtZ_zGHC5AIihBx6AkyM8bJ-p8XwBdMk6nT-SQKO_83DHJh-AtT9qqkIzvFMzMvU5qYTtefZ6yi8Bxwm-Z9Ycu2XABoUNN4_OmuEaEJaGlWGN0V2RAq1xCcKzA"
     *                         ),
     *                     )
     *                 )
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=202,
     *         description="Got action",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="action",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         ),
     *                     )
     *                 )
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=203,
     *         description="Validation error",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="2000"
     *                 ),
     *                 @OA\Property(
     *                     property="error",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="validation failed"
     *                         ),
     *                         @OA\Items(
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  example="username"
     *                              ),
     *                              @OA\Property(
     *                                  property="rule",
     *                                  type="string",
     *                                  example="PresenceOf"
     *                              ),
     *                              @OA\Property(
     *                                  property="detail",
     *                                  type="string",
     *                                  example="The username is required"
     *                              )
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Backend error",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="2000"
     *                 ),
     *                 @OA\Property(
     *                     property="error",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="Something went wrong!"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     ),
     * )
     */
    public function doLogin()
    {
        //username
        //password
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="User registartion",
     *     operationId="doRegistration",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="auth.doRegistration"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="username",
     *                             type="string",
     *                             example="userNew"
     *                         ),
     *                         @OA\Property(
     *                             property="password",
     *                             type="string",
     *                             example="user"
     *                         ),
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="You are registered now. Please login"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function doRegistration()
    {
        //username
        //password
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Verify auth token",
     *     operationId="verifyToken",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="auth.verifyToken"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         ),
     *                         @OA\Property(
     *                             property="auth_session_token",
     *                             type="string",
     *                             example="12345"
     *                         ),
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="id",
     *                             type="integer",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="username",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="token",
     *                             type="string",
     *                             example="Bearer v2.local.sqFFXtZ_zGHC5AIihBx6AkyM8bJ-p8XwBdMk6nT-SQKO_83DHJh-AtT9qqkIzvFMzMvU5qYTtefZ6yi8Bxwm-Z9Ycu2XABoUNN4_OmuEaEJaGlWGN0V2RAq1xCcKzA"
     *                         ),
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function verifyToken()
    {
        //user_id
        //auth_session_token
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Check if user auth or not",
     *     operationId="doCheck",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="auth.doCheck"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="ok"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function doCheck()
    {
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Change password",
     *     operationId="changePassword",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="auth.changePassword"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         ),
     *                         @OA\Property(
     *                             property="password",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="new_password",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="Password has changed"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function changePassword()
    {
//        "user_id": 1,
//        "password": "123456",
//        "new_password": "123456"
    }
}
