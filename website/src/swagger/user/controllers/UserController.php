<?php

namespace MWS\Swagger\User\Controllers;

use MWS\Swagger\MainController;

/**
 * Class UserController
 * @package MWS\Swagger\User\Controllers
 */
class UserController extends MainController
{
    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Get users' profile",
     *     operationId="profile",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="user.profile"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="123456"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="role",
     *                             type="string",
     *                             example="admin"
     *                         ),
     *                         @OA\Property(
     *                             property="permission",
     *                             type="string",
     *                             example="123"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function profile()
    {
       //user_id
    }

    /**
     * @OA\Post(
     *     path="/call",
     *     tags={"user"},
     *     summary="Update users' profile",
     *     operationId="update",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="method",
     *                     type="string",
     *                     example="user.update"
     *                 ),
     *                 @OA\Property(
     *                     property="params",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="user_id",
     *                             type="integer",
     *                             example="1"
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="code",
     *                     type="integer",
     *                     example="1200"
     *                 ),
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(
     *                         @OA\Property(
     *                             property="message",
     *                             type="string",
     *                             example="user update!"
     *                         )
     *                     )
     *                 )
     *             ),
     *         )
     *     )
     * )
     */
    public function update()
    {
        //user_id
    }
}