<?php

namespace MWS\Middleware;

use Phalcon\Mvc\User\Plugin;
use Sid\Phalcon\AuthMiddleware\MiddlewareInterface;

/**
 * Class RedirectOnAuth
 * @package MWS\Middleware
 */
class RedirectOnAuth extends Plugin implements MiddlewareInterface
{
    /**
     * @return bool
     */
    public function authenticate(): bool
    {
        $auth = $this->session->get('auth-session');
        $authMethods = $auth['action']['auth_methods'] ?? ['default'];
        $redirects = [
            '2fa' => [
                'action' => 'fa2Login',
                'to' => '/session/fa2Login'
            ],
            'pgp' => [
                'action' => 'pgpLogin',
                'to' => '/session/pgpLogin'
            ],
            'default' => [
                'action' => 'login',
                'to' => '/session/login'
            ],
        ];
        $action = $this->dispatcher->getActionName();
        if (!in_array($action, array_column($redirects, 'action'))) {
            foreach ($redirects as $ruleName => $redirectRule) {
                if (
                    $action !== $redirects['default']['action'] &&
                    $action !== $redirectRule['action'] &&
                    isset($authMethods[$ruleName]) &&
                    $authMethods[$ruleName] === false) {
                    $this->response->redirect($redirectRule['to']);

                    return false;
                }
            }
        }

        return true;
    }
}