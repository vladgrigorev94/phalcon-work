<?php
namespace MWS\Forms;

use MWS\Traits\ApiValidation;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class PgpLoginForm extends Form
{
    use ApiValidation;

    public function initialize()
    {
        $decodedString = new Text('decodedString', [
            'placeholder' => 'Enter decoded string'
        ]);
        $decodedString->addValidators([
            new PresenceOf([
                'message' => 'decodedString field is required'
            ]),
        ]);
        $this->add($decodedString);

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);

        $this->add(new Submit('Sign In', [
            'class' => 'btn btn-success'
        ]));
    }
}
