<?php

namespace MWS\Forms;

use MWS\Traits\ApiValidation;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;

class Setup2faForm extends Form
{
    use ApiValidation;

    public function initialize($entity = null, $options = null)
    {
        $code = new Text('code');
        $code->setLabel('2fa code');
        $code->addValidators([
            new PresenceOf([
                'message' => 'Field code is required'
            ]),
        ]);
        $this->add($code);

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);

        // Sign Up
        $this->add(new Submit('Send', [
            'class' => 'btn btn-success'
        ]));
    }
}
