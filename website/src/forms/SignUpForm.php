<?php

namespace MWS\Forms;

use MWS\Traits\ApiValidation;
use Phalcon\Filter;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class SignUpForm extends Form
{

    use ApiValidation;

    public function check()
    {
        if ( ! $this->isPostAndValid()) {
            return false;
        }

        $params = [
            'username' => $this->request->getPost('username', 'striptags'),
            'password' => $this->request->getPost('password'),
        ];
        $res = $this->userService->send('auth.doRegistration', $params);

        if ($res->hasError()) {
            $error = $res->getError();
            $this->flash->error($error['message'] ?? 'Unknown error');
            if ( ! empty($error['fields'])) {
                $this->setMessages($error['fields']);
            }

            return false;
        }

        $this->flashSession->success($res->data['message']);

        return true;
    }

    public function initialize($entity = null, $options = null)
    {
        $name = new Text('username');

        $name->setLabel('Username');

        $name->addValidators([
            new PresenceOf([
                'message' => 'The username is required'
            ])
        ]);
        $name->setFilters(
            [
                Filter::FILTER_ALPHANUM,
                Filter::FILTER_TRIM,
            ]
        );
        $this->add($name);
        $password = new Password('password');

        $password->setLabel('Password');

        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ]),
            new StringLength([
                'min'            => 8,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            ]),
            new Confirmation([
                'message' => 'Password doesn\'t match confirmation',
                'with'    => 'confirmPassword'
            ])
        ]);

        $this->add($password);

        // Confirm Password
        $confirmPassword = new Password('confirmPassword');

        $confirmPassword->setLabel('Confirm Password');

        $confirmPassword->addValidators([
            new PresenceOf([
                'message' => 'The confirmation password is required'
            ])
        ]);

        $this->add($confirmPassword);

        // Remember
        $terms = new Check('terms', [
            'value' => 'yes'
        ]);

        $terms->setLabel('Accept terms and conditions');

        $terms->addValidator(new Identical([
            'value'   => 'yes',
            'message' => 'Terms and conditions must be accepted'
        ]));

        $this->add($terms);

        // CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical([
            'value'   => $this->security->getSessionToken(),
            'message' => 'Form expired. Please reload this page.'
        ]));

        $csrf->clear();

        $this->add($csrf);

        // Sign Up
        $this->add(new Submit('Sign Up', [
            'class' => 'btn btn-success'
        ]));
    }
}
