<?php

namespace MWS\Forms;

use MWS\Traits\ApiValidation;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Identical;

class UpdateUserForm extends Form
{
    use ApiValidation;

    public function initialize($entity = null, $options = null)
    {
        $username = new Text('username');
        $username->setLabel('Username');
        $this->add($username);

        $email = new Email('email');
        $email->setLabel('Email');
        $email->addValidators([
            new EmailValidator(
                [
                    'message' => 'The e-mail is not valid',
                ]
            )
        ]);
        $this->add($email);

        $authMethods = $this->permissionComponent->listAuthMethods();
        $authMethod = new Select('authMethod', $authMethods, ['using' => ['name', 'name']]);
        $authMethod->setLabel('Auth method');
        $this->add($authMethod);

        $roles = $this->permissionComponent->listRoles();
        $role = new Select('role', $roles, ['using' => ['id', 'name']]);
        $role->setLabel('Role');
        $this->add($role);

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);

        // Sign Up
        $this->add(new Submit('Send', [
            'class' => 'btn btn-success'
        ]));
    }
}
