<?php

namespace MWS\Traits;

/**
 * Trait ApiValidation
 * @package MWS\Traits
 */
trait ApiValidation
{

    /**
     * @param array $fields
     */
    public function setMessages(array $fields)
    {
        foreach ($fields as $field) {
            $message = new \Phalcon\Validation\Message($field['detail'] ?? 'Unknown');
            if ( ! empty($field['name'])) {
                $this->get($field['name'])->appendMessage($message);
            }
        }
    }

    /**
     * Prints messages for a specific element
     * @param string $name
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

    /**
     * check if request is post and form is valid
     * @return bool
     */
    private function isPostAndValid(): bool
    {
        if ( ! $this->request->isPost()) {
            return false;
        }
        if ( ! $this->isValid($this->request->getPost())) {
            return false;
        }

        return true;
    }
}