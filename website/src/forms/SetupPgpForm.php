<?php

namespace MWS\Forms;

use MWS\Traits\ApiValidation;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;

class SetupPgpForm extends Form
{
    use ApiValidation;

    public function initialize($entity = null, $options = null)
    {
        $publicKey = new TextArea('publicKey');
        $publicKey->setLabel('Public key');
        $publicKey->addValidators([
            new PresenceOf([
                'message' => 'The public key is required'
            ]),
        ]);
        $this->add($publicKey);

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);

        // Sign Up
        $this->add(new Submit('Send', [
            'class' => 'btn btn-success'
        ]));
    }
}
