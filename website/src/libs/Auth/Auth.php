<?php

namespace MWS\Auth;

use Phalcon\Mvc\User\Component;
use ServiceClient\Core\ResponseInterface;
use ServiceClientUser\Services\UserService;

/**
 * Manages Authentication/Identity
 * @property UserService $userService
 */
class Auth extends Component
{
    /**
     * @param array $userData
     * @return ResponseInterface
     * @throws \Exception
     */
    public function login2fa(array $userData): ResponseInterface
    {
        $res = $this->userService->send('multiFactorAuth.do2fa', [
            'action_name' => 'doLogin',
            'auth_session_token' => $userData['authSessionToken'],
            'code' => $userData['code']
        ]);

        return $res;
    }

    /**
     * @param array $userData
     * @return ResponseInterface
     * @throws \Exception
     */
    public function loginPgp(array $userData): ResponseInterface
    {
        $decodedStrng = $userData['decodedString'];
        $res = $this->userService->send('pgpAuth.doPgp', [
            'action_name' => 'doLogin',
            'auth_session_token' => $userData['authSessionToken'],
            'decoded_string' => $decodedStrng
        ]);

        return $res;
    }

    /**
     * @param array $userData
     * @return ResponseInterface
     * @throws \Exception
     */
    public function loginPin(array $userData): ResponseInterface
    {
        $pin = $userData['pin'];
        $res = $this->userService->send('pinAuth.doPin', [
            'action_name' => 'doLogin',
            'auth_session_token' => $userData['authSessionToken'],
            'pin' => $pin
        ]);

        return $res;
    }

    /**
     * Removes the user identity information from session
     */
    public function logout()
    {
        $this->session->remove('auth');
    }


    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->session->get('auth');
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getName()
    {
        $identity = $this->session->get('auth');

        return $identity['username'];
    }

}

