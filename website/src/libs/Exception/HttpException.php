<?php

namespace MWS\Exception;

use Exception;

class HttpException extends Exception implements MWSException
{
}
