<?php

namespace MWS\Exception;

use Phalcon\Exception;

/**
 * MWS\Exception\RuntimeException
 *
 * @package MWS\Exception
 */
class RuntimeException extends Exception
{
}
