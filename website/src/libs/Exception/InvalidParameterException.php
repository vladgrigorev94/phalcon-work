<?php

namespace MWS\Exception;

/**
 * MWS\Exception\InvalidParameterException
 *
 * @package MWS\Exception
 */
class InvalidParameterException extends \InvalidArgumentException
{
}
