<?php

namespace MWS\AfterRouteMiddleware;

interface MiddlewareInterface
{
    public function afterRoute() : bool;
}
