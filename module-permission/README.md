# Docker Phalcon development environment

## Dependencies

- [Docker Engine ~> 18.09.3](https://docs.docker.com/installation/)
- [Docker compose ~> 1.23.2](https://docs.docker.com/compose/install/)

## Usage

    docker-compose up -d --build

    visit http://localhost:8000//permission/call

## Generate Docs

`doxygen Doxyfile` (generated docs folder)

## Action structure

[
    'id' => '12345678',
    'user_id' => 1,
    'created_at' => 2018-12-03 13:12:56,
    'action_name' => 'doLogin',
    'auth_methods' => [
        0 => '2fa',
        1 => 'pgp'
    ],
    'is_completed' => false
]