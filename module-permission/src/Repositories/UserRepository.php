<?php

declare(strict_types=1);

namespace PhPermission\Repositories;

use Phalcon\Di;
use Phalcon\Mvc\ModelInterface;;
use PhPermission\Exceptions\UserNotFoundException;
use PhPermission\Models\AuthMethodModel;
use PhPermission\Models\MultifactorModel;
use PhPermission\Models\PgpModel;
use PhPermission\Models\PinModel;
use PhPermission\Models\UserAuthMethodModel;
use PhPermission\Models\UserModel;

/**
 * Class UserRepository
 * @package PhPermission\Repositories
 */
class UserRepository extends BaseRepository
{

    /**
     * @param int $id
     * @param AuthMethodModel $authMethodModel
     * @return bool
     * @throws \Exception
     */
    public static function setAuthMethodByUserId(int $id, AuthMethodModel $authMethodModel): bool
    {
        $user = self::retrieveById($id);
        $user->authMethods = [$authMethodModel];

        return $user->save();
    }

    /**
     * @param int $userId
     * @param ModelInterface $authMethodModel
     * @return bool
     */
    public static function unsetAuthMethodByUserId(int $userId, ModelInterface $authMethodModel): bool
    {
        $obj = UserAuthMethodModel::query()
            ->where('user_id = :user_id:')
            ->andWhere('auth_method_id = :auth_method_id:')
            ->bind(['user_id' => $userId, 'auth_method_id' => $authMethodModel->id])
            ->execute()
            ->getFirst();
        if (!$obj) {
            return false;
        }
        // @TODO cascade relation UserAuthMethodModel
        switch ($authMethodModel->name) {
            case AuthMethodModel::AUTH_METHOD_PGP:
                PgpModel::query()
                    ->where('user_id = :user_id:')
                    ->bind(['user_id' => $userId])
                    ->execute()
                    ->getFirst()
                    ->delete();
                break;
            case AuthMethodModel::AUTH_METHOD_PIN:
                PinModel::query()
                    ->where('user_id = :user_id:')
                    ->bind(['user_id' => $userId])
                    ->execute()
                    ->getFirst()
                    ->delete();
                break;
            case AuthMethodModel::AUTH_METHOD_2FA:
                MultifactorModel::query()
                    ->where('user_id = :user_id:')
                    ->bind(['user_id' => $userId])
                    ->execute()
                    ->getFirst()
                    ->delete();
                break;
        }

        return $obj->delete();
    }

    /**
     * @param int $id
     * @param string $encryptedString
     * @param string $hash
     * @return bool
     * @throws \Exception
     */
    public static function setPgp(int $id, string $encryptedString, string $hash): bool
    {
        $user = self::retrieveById($id);
        $pgp = new PgpModel();
        $pgp->setPGP($encryptedString, $hash);
        /**
         * don't understand why but I can't save pgp without set user_id
         */
        $pgp->user_id = $id;
        $user->pgp = $pgp;

        return $user->save();
    }

    /**
     * @param int $id
     * @param string $hash
     * @return bool
     * @throws \Exception
     */
    public static function verifyPgpHash(int $id, string $hash): bool
    {
        $user = self::retrieveById($id);

        return $hash === $user->pgp->pgp_decrypt_hash;
    }

    /**
     * @param int $id
     * @param string $hash
     * @return bool
     * @throws \Exception
     */
    public static function setPin(int $id, string $hash): bool
    {
        $user = self::retrieveById($id);
        $service = Di::getDefault();
        $security = $service->get('security');
        $pin = new PinModel();
        /**
         * don't understand why but I can't save pgp without set user_id
         */
        $pin->user_id = $id;
        $pin->pin = $security->hash($hash);
        $user->pin = $pin;

        return $user->save();
    }

    /**
     * @param int $id
     * @param string $pin
     * @return bool
     * @throws \Exception
     */
    public static function verifyPin(int $id, string $pin): bool
    {
        $user = self::retrieveById($id);
        $service = Di::getDefault();
        $security = $service->get('security');

        return $security->checkHash(trim($pin), $user->pin->pin);
    }

    /**
     * @param int $userId
     * @param int $roleId
     * @return bool
     * @throws \Exception
     */
    public static function updateRoleById(int $userId, int $roleId): bool
    {
        $user = UserModel::getUser($userId);
        $user->setRole($roleId);

        return $user->save();
    }

    /**
     * @return mixed|string
     */
    public static function getModel()
    {
        return UserModel::class;
    }

    /**
     * @return mixed|string
     */
    public static function getException()
    {
        return UserNotFoundException::class;
    }
}