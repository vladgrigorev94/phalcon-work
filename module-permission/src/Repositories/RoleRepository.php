<?php

declare(strict_types=1);

namespace PhPermission\Repositories;

use PhPermission\Exceptions\RoleNotFoundException;
use PhPermission\Models\RoleModel;

/**
 * Class RoleRepository
 * @package PhPermission\Repositories
 */
class RoleRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    public static function getModel()
    {
        return RoleModel::class;
    }

    /**
     * @return mixed|string
     */
    public static function getException()
    {
        return RoleNotFoundException::class;
    }
}