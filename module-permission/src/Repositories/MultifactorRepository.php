<?php

declare(strict_types=1);

namespace PhPermission\Repositories;

use Phalcon\Mvc\ModelInterface;
use PhPermission\Exceptions\MultifactorNotFoundException;
use PhPermission\Models\MultifactorModel;

/**
 * Class MultifactorRepository
 * @package PhPermission\Repositories
 */
class MultifactorRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    public static function getModel()
    {
        return MultifactorModel::class;
    }

    /**
     * @return mixed|string
     */
    public static function getException()
    {
        return MultifactorNotFoundException::class;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function retrieveByUserId(int $id)
    {
        return MultifactorModel::query()
            ->where('user_id = :id:')
            ->bind(['id' => $id])
            ->execute()
            ->getFirst();
    }

    /**
     * @param int $id
     * @return string|null
     * @throws \Exception
     */
    public static function getGoogleAuthSecret(int $id): ?string
    {
        $multifactor = self::retrieveByUserId($id);
        $exception = static::getException();
        if (empty($multifactor)) {
            throw new $exception('record not found');
        }

        return $multifactor->google_auth_secret;
    }

    /**
     * @param int $userId
     * @param string $googleAuthSecret
     * @return bool
     */
    public static function setGoogleAuthSecret(int $userId, string $googleAuthSecret): bool
    {
        $multifactor = self::retrieveByUserId($userId);
        if (empty($multifactor)) {
            $multifactor = new MultifactorModel();
            $multifactor->user_id = $userId;
            $multifactor->google_auth_secret = $googleAuthSecret;

            return $multifactor->save();
        }
        else {
            $db = \Phalcon\DI::getDefault()->get('db');
            $statement  = $db->prepare('UPDATE user_multifactor SET google_auth_secret = :gauth WHERE user_id = :uid');
            $statement->bindParam(':uid', $userId);
            $statement->bindParam(':gauth', $googleAuthSecret);
            $statement->execute();

            return true;
        }
    }

}