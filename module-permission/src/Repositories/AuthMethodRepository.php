<?php

declare(strict_types=1);

namespace PhPermission\Repositories;

use Phalcon\Mvc\ModelInterface;
use PhPermission\Exceptions\AuthMethodNotFoundException;
use PhPermission\Models\AuthMethodModel;

/**
 * Class AuthMethodRepository
 * @package PhPermission\Repositories
 */
class AuthMethodRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    public static function getModel()
    {
        return AuthMethodModel::class;
    }

    /**
     * @return mixed|string
     */
    public static function getException()
    {
        return AuthMethodNotFoundException::class;
    }

    /**
     * @param string $name
     * @return AuthMethodModel
     */
    public static function getByName(string $name): ModelInterface
    {
        return AuthMethodModel::query()
            ->where('name = :name:')
            ->bind(['name' => $name])
            ->execute()
            ->getFirst();
    }
}