<?php

declare(strict_types=1);

namespace PhPermission\Repositories;

use Phalcon\Mvc\ModelInterface;

/**
 * Class BaseRepository
 * @package PhPermission\Repositories
 */
abstract class BaseRepository
{
    /**
     * @param int $id
     * @return ModelInterface|null
     * @throws \Exception
     */
    public static function retrieveById(int $id): ?ModelInterface
    {
        $obj = static::getModel()::query()
            ->where('id = :id:')
            ->bind(['id' => $id])
            ->execute()
            ->getFirst();

        $exception = static::getException();
        if (!$id || !$obj) {
            throw new $exception('model not found');
        }

        return $obj;
    }

    /**
     * @return mixed
     */
    abstract public static function getModel();

    /**
     * @return mixed
     */
    abstract public static function getException();
}