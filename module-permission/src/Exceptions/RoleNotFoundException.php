<?php

namespace PhPermission\Exceptions;

/**
 * Class RoleNotFoundException
 * @package PhPermission\Exceptions
 */
class RoleNotFoundException extends \Exception
{
}

