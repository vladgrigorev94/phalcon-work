<?php

namespace PhPermission\Exceptions;

/**
 * Class AuthMethodNotFoundException
 * @package PhPermission\Exceptions
 */
class AuthMethodNotFoundException extends \Exception
{
}

