<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;

class GetGoogleAuthSecretValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);
    }
}
