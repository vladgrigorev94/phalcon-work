<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;

/**
 * Class SetupAuthMethodValidation
 * @package PhPermission\Validation
 */
class SetupAuthMethodValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfActionName = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field action_name is required',
            ]
        );
        $this->add('action_name', $presenceOfActionName);

        $presenceOfAuthMethod = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field auth_method is required',
            ]
        );
        $this->add('auth_method', $presenceOfAuthMethod);

        $presenceOfAuthSessionToken = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field auth_session_token is required',
            ]
        );
        $this->add('auth_session_token', $presenceOfAuthSessionToken);
    }
}
