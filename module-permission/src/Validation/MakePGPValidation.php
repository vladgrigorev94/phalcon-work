<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;

class MakePGPValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfPublicKey = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field public_key is required',
            ]
        );
        $this->add('public_key', $presenceOfPublicKey);

    }
}
