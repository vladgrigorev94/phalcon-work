<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class UnsetAuthMethodValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );

        $this->add('user_id', $presenceOfUserId);


        $presenceOfAuthMethodId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field auth_method_id is required',
            ]
        );
        $this->add('auth_method_id', $presenceOfAuthMethodId);
    }
}
