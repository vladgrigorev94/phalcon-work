<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;

/**
 * Class CheckActionCompleteValidation
 * @package PhPermission\Validation
 */
class CheckActionCompleteValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfActionName = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field action name is required',
            ]
        );
        $this->add('action_name', $presenceOfActionName);

        $presenceOfAuthSessionToken = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field auth_session_token is required',
            ]
        );
        $this->add('auth_session_token', $presenceOfAuthSessionToken);
    }
}
