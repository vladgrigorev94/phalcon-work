<?php

declare(strict_types=1);

namespace PhPermission\Validation;

use Phalcon\Validation;

class SetGoogleAuthSecretValidation extends Validation
{
    public function initialize()
    {
        $presenceOfUserId = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field user_id is required',
            ]
        );
        $this->add('user_id', $presenceOfUserId);

        $presenceOfGoogleAuthSecret = new \Phalcon\Validation\Validator\PresenceOf(
            [
                'message' => 'Field google_auth_secret is required',
            ]
        );
        $this->add('google_auth_secret', $presenceOfGoogleAuthSecret);
    }
}
