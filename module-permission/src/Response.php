<?php

namespace PhPermission;

use Phalcon\Mvc\User\Plugin;

class Response extends Plugin {

    protected $head = false;

    public function __construct() {
        if (strtolower($this->di->get('request')->getMethod()) === 'head') {
            $this->head = true;
        }
    }

    public function send($records, $error = false) {

        // Error's come from HTTPException.  This helps set the proper envelope data
        //$this->response = $this->di->get('response');
        $message = [];
        if($error){
            $message['type'] = 'error';
            $message['error'] = $records;
        } else {
            if (isset($records['data'])) {
                $message['data'] = $records['data'];
            } else {
                $message['data'] = $records;
            }
        }


        $this->response->setContentType('application/json');

        // HEAD requests are detected in the parent constructor. HEAD does everything exactly the
        // same as GET, but contains no body.
        if (!$this->head) {
            $this->response->setJsonContent($message, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        }

        $this->response->send();

        return $this;
    }
}
