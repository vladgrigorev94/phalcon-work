<?php

namespace PhPermission\Controllers;

use Phalcon\Mvc\Controller;
use PhCore\Http\Response;
use PhPermission\Models\RoleModel;
use PhPermission\Models\UserRoleModel;
use PhPermission\Repositories\UserRepository;
use PhPermission\Validation\RoleUpdateValidation;
use PhPermission\Validation\RoleValidation;

/**
 * Class RoleController
 * @package PhPermission\Controllers
 */
class RoleController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $roles = RoleModel::find();

        return $this
            ->response
            ->setPayloadSuccess(['roles' => $roles]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function show()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new RoleValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $user = UserRepository::retrieveById($params->user_id);
        $role = $user->roles->getFirst();
        if (empty($role)) {
            return $this
                ->response
                ->setPayloadError('user has no role. please fix it');
        }

        return $this
            ->response
            ->setPayloadSuccess(['role' => $role->name]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function update()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new RoleUpdateValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        UserRepository::updateRoleById($params->user_id, $params->role_id);

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function delete()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new RoleValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $user = UserRepository::retrieveById($params->user_id);
        $user->userRoles->delete();

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!']);
    }
}