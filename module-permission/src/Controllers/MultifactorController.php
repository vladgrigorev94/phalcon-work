<?php

namespace PhPermission\Controllers;

use Phalcon\Mvc\Controller;
use PhPermission\Repositories\MultifactorRepository;
use PhPermission\Validation\GetGoogleAuthSecretValidation;
use PhPermission\Validation\SetGoogleAuthSecretValidation;

/**
 * Class MultifactorController
 * @package PhPermission\Controllers
 */
class MultifactorController extends Controller
{
    /**
     * @return mixed
     * @throws \Exception
     */
    public function getGoogleAuthSecret()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new GetGoogleAuthSecretValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        if (empty($gAuth = MultifactorRepository::getGoogleAuthSecret($params['user_id']))) {
            return $this
                ->response
                ->setPayloadError('couldn\'t get google_auth_secret');
        }

        return $this
            ->response
            ->setPayloadSuccess(['google_auth_secret' => $gAuth]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function setGoogleAuthSecret()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new SetGoogleAuthSecretValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        if (!MultifactorRepository::setGoogleAuthSecret($params['user_id'], $params['google_auth_secret'])) {
            return $this
                ->response
                ->setPayloadError('cant save google auth');
        }

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!']);
    }

}