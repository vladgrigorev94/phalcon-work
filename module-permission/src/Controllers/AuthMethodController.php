<?php

namespace PhPermission\Controllers;

use Phalcon\Mvc\Controller;
use PhPermission\Models\AuthMethodModel;
use PhPermission\Repositories\AuthMethodRepository;
use PhPermission\Repositories\MultifactorRepository;
use PhPermission\Repositories\UserRepository;
use PhPermission\Validation\AuthMethodActiveValidation;
use PhPermission\Validation\AuthMethodValidation;
use PhPermission\Validation\DisableAuthMethodValidation;
use PhPermission\Validation\GetGoogleAuthSecretValidation;
use PhPermission\Validation\SetAuthMethodValidation;
use PhPermission\Validation\SetGoogleAuthSecretValidation;
use PhPermission\Validation\UnsetAuthMethodValidation;

class AuthMethodController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $authMethods = AuthMethodModel::find();

        return $this
            ->response
            ->setPayloadSuccess(['auth_methods' => $authMethods]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function show()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new AuthMethodValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $user = UserRepository::retrieveById($params->user_id);
        $authMethods = $user->authMethods->toArray();

        return $this
            ->response
            ->setPayloadSuccess(['auth_methods' => $authMethods]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function setAuthMethod()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new SetAuthMethodValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $authMethod = AuthMethodRepository::getByName($params->auth_method);
        UserRepository::setAuthMethodByUserId($params->user_id, $authMethod);

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function disableAuthMethod()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new DisableAuthMethodValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $authMethod = AuthMethodRepository::retrieveById($params->auth_method_id);
        if (!UserRepository::unsetAuthMethodByUserId($params->user_id, $authMethod)) {
            return $this
                ->response
                ->setPayloadError('couldn\'t unset');
        }

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function unsetAuthMethod()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new UnsetAuthMethodValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $authMethod = AuthMethodRepository::retrieveById($params->auth_method_id);
        if (!UserRepository::unsetAuthMethodByUserId($params->user_id, $authMethod)) {
            return $this
                ->response
                ->setPayloadError('couldn\'t unset');
        }

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function setAuthMethodActive()
    {
        $params = $this->params;
        if (!$this->validation->run($params->toArray(), new AuthMethodActiveValidation())) {
            return $this
                ->response
                ->setValidationError($this->validation->getMessages());
        }
        $authMethod = MultifactorRepository::retrieveById($params->auth_method_id);
        $authMethod->active = $params->active;
        $authMethod->save();

        return $this
            ->response
            ->setPayloadSuccess(['message' => 'Success!']);
    }

}