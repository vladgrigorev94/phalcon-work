<?php

namespace PhPermission\Services;

use Crypt_GPG;

/**
 * Class PgpService
 * @package PhPermission\Services
 */
class PgpService
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * PgpService constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $pubkey
     * @param string $hashString
     * @return string
     * @throws \Crypt_GPG_BadPassphraseException
     * @throws \Crypt_GPG_Exception
     * @throws \Crypt_GPG_FileException
     * @throws \Crypt_GPG_KeyNotFoundException
     * @throws \Crypt_GPG_NoDataException
     * @throws \PEAR_Exception
     */
    public function generatePGP(string $pubkey, string $hashString = '')
    {
        $gpg = new Crypt_GPG([
            'homedir' => $this->config['pgp']['homedir']
        ]);
        unset($_ENV['argv']);
        $res = $gpg->importKey($pubkey);
        if(empty($res)) {
            throw new \Exception('Cannot import public key');
        }
        $res = $gpg->addEncryptKey($res['fingerprint']);
        if(empty($res)) {
            throw new \Exception('Cannot import fingerprint');
        }

        $encryptedString = $gpg->encrypt($hashString);
        if(empty($encryptedString)) {
            throw new \Exception('Encrypted string wasn\'t created');
        }

        return $encryptedString;
    }
}

