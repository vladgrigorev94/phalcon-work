<?php

namespace PhPermission\Services;

use Phalcon\Cache\BackendInterface;
use PhPermission\Models\AuthMethodModel;
use PhPermission\Models\UserAuthMethodModel;

/**
 * Class ActionService
 * @package PhPermission\Services
 */
class ActionService
{
    /**
     * @var BackendInterface
     */
    private $cache;

    /**
     * ActionService constructor.
     * @param BackendInterface $cache
     */
    public function __construct(BackendInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param int $userId
     * @param string $actionName
     * @param array $authMethods
     * @param string $authSessionToken
     * @param int|null $lifetime
     * @return bool
     */
    private function saveAction(int $userId, string $actionName, array $authMethods, string $authSessionToken, int $lifetime = null): bool
    {
        $userAuthMethods = UserAuthMethodModel::query()
            ->where('user_id = :user_id:')
            ->bind(['user_id' => $userId])
            ->execute();
        $userAuthMethodsArr = [];
        foreach ($userAuthMethods as $userAuthMethod) {
            if ($userAuthMethod->authMethod->name !== AuthMethodModel::AUTH_METHOD_DEFAULT) {
                $userAuthMethodsArr[] = $userAuthMethod->authMethod->name;
            }
        }
        //we make an array with equal values: user's auth methods and ours
        $authMethodsArr = array_intersect($authMethods, $userAuthMethodsArr);
        $authMethodsArr = array_combine($authMethodsArr, array_fill(0, count($authMethodsArr), false));
        $action = [
            'id' => md5(rand()),
            'user_id' => $userId,
            'created_at' => date('Y-m-d H:i:s'),
            'action_name' => $actionName,
            'auth_methods' => $authMethodsArr,
            'is_completed' => false
        ];

        return $this->cache->save($actionName . '_' . $userId . '_' . $authSessionToken,
            serialize($action),
            $lifetime
        );
    }

    /**
     * @param int $userId
     * @param string $actionName
     * @param string $authSessionToken
     * @return array
     */
    public function getAction(int $userId, string $actionName, string $authSessionToken): array
    {
        return unserialize($this->cache->get($actionName . '_' . $userId . '_' . $authSessionToken));
    }

    /**
     * @param int $userId
     * @param string $actionName
     * @param array $authMethods
     * @param string $authSessionToken
     * @return array
     * @throws \Exception
     */
    public function generate(int $userId, string $actionName, array $authMethods, string $authSessionToken): array
    {
        if (!is_array($authMethods)) {
            throw new \Exception('invalid auth_methods structure');
        }
        if (!$this->cache->exists($actionName . '_' . $userId . '_' . $authSessionToken)) {
            $this->saveAction($userId, $actionName, $authMethods, $authSessionToken);
        }
        $action = $this->getAction($userId, $actionName, $authSessionToken);

        return $action;
    }

    /**
     * @param int $userId
     * @param string $actionName
     * @param string $authMethodName
     * @param string $authSessionToken
     * @return bool
     * @throws \Exception
     */
    public function setPassActionAuthMethod(int $userId, string $actionName, string $authMethodName, string $authSessionToken): bool
    {
        if (!$this->cache->exists($actionName . '_' . $userId . '_' . $authSessionToken)) {
            throw new \Exception('there is no same action in db');
        }
        $action = $this->getAction($userId, $actionName, $authSessionToken);
        if (!isset($action['auth_methods'][$authMethodName])) {
            throw new \Exception('there is no same auth method name on that action');
        }
        $action['auth_methods'][$authMethodName] = true;
        if (!$this->cache->save($actionName . '_' . $userId . '_' . $authSessionToken, serialize($action))) {
            throw new \Exception('couldn\'t refresh action\'s auth method');
        }

        return true;
    }

    /**
     * @param int $userId
     * @param string $actionName
     * @param string $authSessionToken
     * @return bool
     * @throws \Exception
     */
    public function checkActionComplete(int $userId, string $actionName, string $authSessionToken): bool
    {
        if (!$this->cache->exists($actionName . '_' . $userId . '_' . $authSessionToken)) {
            throw new \Exception('there is no same action in db');
        }
        $action = $this->getAction($userId, $actionName, $authSessionToken);
        if (in_array(false, $action['auth_methods'], true)) {
            return false;
        }
        $this->cache->delete($actionName . '_' . $userId . '_' . $authSessionToken);

        return true;
    }

    /**
     * @param int $userId
     * @param float|int $lifetime
     * @return string
     * @throws \Exception
     */
    public function generateAuthSessionToken(int $userId, $lifetime = 60 * 15): string
    {
        $sessionToken = md5(rand());
        if (!$this->cache->save($sessionToken, $userId, $lifetime)) {
            throw new \Exception('couldn\'t save auth session token');
        }

        return $sessionToken;
    }

    /**
     * @param string $authSessionToken
     * @return string
     * @throws \Exception
     */
    public function getUserIdByAuthSessionToken(string $authSessionToken): string
    {
        $userId = $this->cache->get($authSessionToken);
        if (empty($userId)) {
            throw new \Exception('user not found by auth session token');
        }

        return $userId;
    }

    /**
     * @param string $authSessionToken
     * @throws \Exception
     */
    public function dropAuthSessionToken(string $authSessionToken): void
    {
        if (!$this->cache->delete($authSessionToken)) {
            throw new \Exception('couldn\'t drop auth session token');
        }
    }
}

