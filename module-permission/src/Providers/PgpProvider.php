<?php

declare(strict_types=1);

namespace PhPermission\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use PhPermission\Services\PgpService;

/**
 * Class PgpProvider
 * @package PhPermission\Providers
 */
class PgpProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'pgp',
            function () use($container){
                $config = $container->get('config')->toArray();

                return new PgpService($config);
            }
        );
    }
}