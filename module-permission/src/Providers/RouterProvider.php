<?php

declare(strict_types=1);

namespace PhPermission\Providers;

use PhCore\Middleware\AuthenticationMiddleware;
use PhCore\Middleware\NotFoundMiddleware;
use PhCore\Middleware\ResponseMiddleware;
use PhCore\Providers\RouterProviderAbstract;

class RouterProvider extends RouterProviderAbstract
{

    /**
     * Returns the array for the middleware with the action to attach
     *
     * @return array
     */
    function getMiddleware(): array
    {
        return [
            NotFoundMiddleware::class => 'before',
            AuthenticationMiddleware::class => 'before',
            ResponseMiddleware::class => 'after'
        ];
    }
}
