<?php

declare(strict_types=1);

namespace PhPermission\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Acl\Adapter\Memory as AclList;

class AclProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'acl',
            function () {
                $acl = new AclList();

                return $acl;
            }
        );
    }
}
