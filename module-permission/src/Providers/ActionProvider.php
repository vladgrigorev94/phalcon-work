<?php

declare(strict_types=1);

namespace PhPermission\Providers;

use Phalcon\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use PhPermission\Services\ActionService;

/**
 * Class ActionProvider
 * @package PhPermission\Providers
 */
class ActionProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container)
    {
        $container->setShared(
            'action',
            function () use ($container) {
                $frontend = new \Phalcon\Cache\Frontend\Data([
                    'lifetime' => 86400,
                    'prefix' => 'phalcon_permission.action'
                ]);
                $cache = new \Phalcon\Cache\Backend\Redis($frontend, [
                    'host' => getenv('REDIS_HOST'),
                    'port' => getenv('REDIS_PORT'),
                    'persistent' => false,
                    'prefix' => 'phalcon_permission.action',
                    'index' => 0
                ]);

                return new ActionService($cache);
            }
        );


    }
}