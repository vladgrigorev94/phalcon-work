<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class MultifactorModel
 * @package PhPermission\Models
 */
class MultifactorModel extends Model
{
    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=32, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(column="google_auth_secret", type="string", nullable=true)
     */
    public $google_auth_secret;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('user_multifactor');

        $this->hasOne(
            'id',
            'PhPermission\Models\UserModel',
            'user_id',
            [
                'alias' => 'UserModel'
            ]
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_multifactor';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'user_id', 'google_auth_secret'
        ];
    }
}
