<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class RoleModel
 * @package PhPermission\Models
 */
class RoleModel extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", nullable=false)
     */
    public $name;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('roles');

        $this->hasMany('id', UserRoleModel::class, 'role_id');
        $this->hasManyToMany(
            'id',
            RolePermissionModel::class,
            'role_id', 'permission_id',
            PermissionModel::class,
            'id'
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'roles';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'name'
        ];
    }
}
