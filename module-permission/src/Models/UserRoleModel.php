<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class UserRoleModel
 * @package PhPermission\Models
 */
class UserRoleModel extends Model
{
    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=32, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(column="role_id", type="integer", length=32, nullable=false)
     */
    public $role_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('user_roles');

        $this->belongsTo(
            'role_id',
            'PhPermission\Models\RoleModel',
            'user_id',
            [
                'alias'    => 'Role',
            ]
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_roles';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'user_id',
            'role_id',
        ];
    }
}
