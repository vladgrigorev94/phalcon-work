<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class PinModel
 * @package PhPermission\Models
 */
class PinModel extends Model
{
    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=32, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(column="pin", type="string", nullable=true)
     */
    public $pin;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('user_pins');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_pins';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'user_id', 'pin'
        ];
    }
}
