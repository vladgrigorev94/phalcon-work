<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class ModulePartModel
 * @package PhPermission\Models
 */
class ModulePartModel extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=32, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="module_id", type="integer", length=32, nullable=false)
     */
    public $module_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", nullable=false)
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('modules_parts');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'modules_parts';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'module_id',
            'name'
        ];
    }
}
