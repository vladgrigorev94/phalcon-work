<?php

namespace PhPermission\Models;

use Phalcon\Mvc\Model;

/**
 * Class PgpModel
 * @package PhPermission\Models
 */
class PgpModel extends Model
{
    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=32, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(column="pgp_encrypt_hash", type="string", nullable=true)
     */
    public $pgp_encrypt_hash;

    /**
     *
     * @var string
     * @Column(column="pgp_decrypt_hash", type="string", nullable=true)
     */
    public $pgp_decrypt_hash;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('public');
        $this->setSource('user_pgp');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_pgp';
    }

    /**
     * @return array
     */
    public static function allowedFields()
    {
        return [
            'user_id', 'pgp_encrypt_hash', 'pgp_decrypt_hash'
        ];
    }

    /**
     * @param string $enc
     * @param string $hash
     */
    public function setPGP(string $enc, string $hash)
    {
        $this->pgp_encrypt_hash = $enc;
        $this->pgp_decrypt_hash = $hash;

        $this->save();
    }
}
