<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//(new \Phalcon\Debug)->listen();
//ini_set('phalcon.orm.not_null_validations', 'on');
define('BASE_DIR', dirname(__DIR__));
define('APP_NAME', 'module-permission');
require_once BASE_DIR . '/vendor/autoload.php';
error_reporting(E_ALL);
$bootstrap = new PhCore\Bootstrap\Api();
$bootstrap->setup();
$bootstrap->run();