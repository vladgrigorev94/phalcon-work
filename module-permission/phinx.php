<?php

require_once 'vendor/autoload.php';

if (is_file( '.env')) {
    (Dotenv\Dotenv::create('./'))->load();
}
//$config = include 'config/config.php';
$dbname = getenv('POSTGRES_DB');
return [
    "paths"        => [
        "migrations" => "db/migrations",
        "seeds"      => "db/seeds",
    ],
    "environments" => [
        "default_migration_table" => "phinxlog",
        "default_database"        => $dbname,
        $dbname                  => [
            "adapter" => "pgsql",
            "host"    => getenv('DB_HOST'),
            "name"    => getenv('POSTGRES_DB'),
            "user"    => getenv('POSTGRES_USER'),
            "pass"    => getenv('POSTGRES_PASSWORD'),
            "port"    => getenv('POSTGRES_PORT'),
            "charset" => "utf8"
        ]
    ]
];