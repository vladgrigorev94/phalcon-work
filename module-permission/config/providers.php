<?php

/**
 * Enabled providers. Order does matter
 */

use PhCore\Providers\DatabaseProvider;
use PhCore\Providers\ErrorHandlerProvider;
use PhCore\Providers\LoggerProvider;
use PhCore\Providers\ModelsMetadataProvider;
use PhCore\Providers\RequestProvider;
use PhCore\Providers\ResponseProvider;
use PhCore\Providers\ValidationProvider;
use PhPermission\Providers\AclProvider;
use PhPermission\Providers\ActionProvider;
use PhPermission\Providers\ConfigProvider;
use PhPermission\Providers\PgpProvider;
use PhPermission\Providers\RouterProvider;

return [
    ConfigProvider::class,
    LoggerProvider::class,
    RequestProvider::class,
    ResponseProvider::class,
    ErrorHandlerProvider::class,
    DatabaseProvider::class,
    ModelsMetadataProvider::class,
    AclProvider::class,
    RouterProvider::class,
    PgpProvider::class,
    ValidationProvider::class,
    ActionProvider::class
];