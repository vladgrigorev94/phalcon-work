<?php

use Phalcon\Security;
use Phinx\Seed\AbstractSeed;

class AuthMethodSeeder extends AbstractSeed
{

    public function run()
    {

        $data = [
            [
                'id' => 1
            ],
            [
                'id' => 2
            ],
            [
                'id' => 3
            ],
            [
                'id' => 4
            ],
            [
                'id' => 5
            ],
            [
                'id' => 6
            ],
            [
                'id' => 7
            ],
            [
                'id' => 8
            ],
            [
                'id' => 9
            ]
        ];

        $entity = $this->table('users');
        $entity->insert($data)->save();

        $data = [
            [
                'name' => 'default'
            ],
            [
                'name' => '2fa'
            ],
            [
                'name' => 'pgp'
            ],
            [
                'name' => 'pin'
            ]
        ];

        $entity = $this->table('auth_methods');
        $entity->insert($data)->save();

        $data = [
            [
                'user_id' => 1,
                'auth_method_id' => 1,
            ],
            [
                'user_id' => 2,
                'auth_method_id' => 2,
            ],
            [
                'user_id' => 3,
                'auth_method_id' => 3,
            ],
            [
                'user_id' => 4,
                'auth_method_id' => 4,
            ],
            [
                'user_id' => 5,
                'auth_method_id' => 2,
            ],
            [
                'user_id' => 6,
                'auth_method_id' => 2,
            ],
            [
                'user_id' => 7,
                'auth_method_id' => 1,
            ],
            [
                'user_id' => 8,
                'auth_method_id' => 3,
            ],
            [
                'user_id' => 9,
                'auth_method_id' => 4,
            ]
        ];
        $entity = $this->table('users_auth_methods');
        $entity->insert($data)->save();
    }

}
