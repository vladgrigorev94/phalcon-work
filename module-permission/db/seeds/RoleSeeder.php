<?php

use Phalcon\Security;
use Phinx\Seed\AbstractSeed;

class RoleSeeder extends AbstractSeed
{

    public function run()
    {
        $data = [
            [
                'name' => 'Admin'
            ],
            [
                'name' => 'User'
            ],
            [
                'name' => 'Courier'
            ],
            [
                'name' => 'Seller'
            ],
            [
                'name' => 'Employer'
            ]
        ];

        $entity = $this->table('roles');
        $entity->insert($data)->save();

        $data = [
            [
                'user_id' => 1,
                'role_id' => 1,
            ],
            [
                'user_id' => 2,
                'role_id' => 2,
            ],
            [
                'user_id' => 3,
                'role_id' => 2,
            ],
            [
                'user_id' => 4,
                'role_id' => 2,
            ],
            [
                'user_id' => 5,
                'role_id' => 2,
            ],
            [
                'user_id' => 6,
                'role_id' => 2,
            ],
            [
                'user_id' => 7,
                'role_id' => 2,
            ],
            [
                'user_id' => 8,
                'role_id' => 2,
            ],
            [
                'user_id' => 9,
                'role_id' => 2,
            ]
        ];
        $entity = $this->table('user_roles');
        $entity->insert($data)->save();
    }

}
