<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class AuthMethodMigration extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('auth_methods', [
            'signed' => false
        ]);
        $table->addColumn('name',  Literal::from('varchar'))
            ->create();
        $table = $this->table('users_auth_methods', ['signed' => false]);
        $table->addColumn('user_id', 'integer')
            ->addColumn('auth_method_id', 'integer')
            ->create();
        $table->changePrimaryKey(['user_id', 'auth_method_id']);
        $table->removeColumn('id');
        $table->addForeignKey('auth_method_id', 'auth_methods');
        $table->save();
    }
}
