<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{

    public function change()
    {
        $table = $this->table('users', ['signed' => false]);
        $table->create();
        $table->removeColumn('id');
        $table
            ->addColumn('id', 'integer', [
                'identity' => false
            ])
            ->changePrimaryKey(['id'])
            ->save();

        $table = $this->table('user_pgp');
        $table->addForeignKey('user_id', 'users');
        $table->save();

        $table = $this->table('user_pins');
        $table->addForeignKey('user_id', 'users');
        $table->save();

        $table = $this->table('users_auth_methods');
        $table->addForeignKey('user_id', 'users');
        $table->save();

        $table = $this->table('user_roles');
        $table->addForeignKey('user_id', 'users');
        $table->save();
    }

}
