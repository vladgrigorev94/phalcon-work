<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class AddPgpTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('user_pgp', ['signed' => false]);
        $table
            ->addColumn('user_id', 'integer')
            ->addColumn('pgp_encrypt_hash', 'text', ['null' => true])
            ->addColumn('pgp_decrypt_hash',  Literal::from('varchar'), ['null' => true])
            ->create();

        $table->changePrimaryKey(['user_id']);
        $table->removeColumn('id');
        $table->addIndex(['pgp_encrypt_hash'], ['unique' => true]);
        $table->addIndex(['pgp_decrypt_hash'], ['unique' => true]);
        $table->save();
    }
}
