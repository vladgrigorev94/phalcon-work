<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreatePin extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('user_pins', ['signed' => false]);
        $table
            ->addColumn('user_id', 'integer')
            ->addColumn('pin', Literal::from('varchar'), ['null' => true])
            ->create();

        $table->changePrimaryKey(['user_id']);
        $table->removeColumn('id');
        $table->save();
    }
}
