<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

class CreateUserMultifactorTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('user_multifactor', ['signed' => false]);
        $table
            ->addColumn('user_id', 'integer')
            ->addColumn('google_auth_secret',  Literal::from('varchar'),
                [
                    'null' => true
                ])
            ->addIndex(['user_id'], ['unique' => true])
            ->addIndex(['google_auth_secret'], ['unique' => true])
            ->create();
        $table = $this->table('user_multifactor');
        $table->addForeignKey('user_id', 'users');
        $table->changePrimaryKey(['user_id']);
        $table->removeColumn('id');
        $table->save();
    }
}
